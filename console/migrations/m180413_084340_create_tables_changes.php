<?php

use yii\db\Migration;

/**
 * Class m180413_084340_create_tables_changes
 */
class m180413_084340_create_tables_changes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('orders','address_details','district');
        $this->alterColumn('orders', 'district', $this->string()->null());
        $this->dropColumn('orders', 'house_number');
        $this->addColumn('user_details', 'apartment_number', $this->string()->after('address'));
        $this->addColumn('user_details', 'district', $this->string()->after('apartment_number'));
        $this->addColumn('user', 'secret_key', $this->string());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180413_084340_create_tables_changes cannot be reverted.\n";
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180413_084340_create_tables_changes cannot be reverted.\n";

        return false;
    }
    */
}
