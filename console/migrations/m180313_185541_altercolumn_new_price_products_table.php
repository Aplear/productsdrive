<?php

use yii\db\Migration;

/**
 * Class m180313_185541_altercolumn_new_price_products_table
 */
class m180313_185541_altercolumn_new_price_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('products', 'new_price',$this->double()->after('price'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180313_185541_altercolumn_new_price_products_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180313_185541_altercolumn_new_price_products_table cannot be reverted.\n";

        return false;
    }
    */
}
