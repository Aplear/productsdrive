<?php

use yii\db\Migration;

/**
 * Class m180411_200457_add_column_price_fee_products_table
 */
class m180411_200457_add_column_price_fee_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('products', 'price_fee', $this->float()->defaultValue(0)->after('price'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180411_200457_add_column_price_fee_products_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180411_200457_add_column_price_fee_products_table cannot be reverted.\n";

        return false;
    }
    */
}
