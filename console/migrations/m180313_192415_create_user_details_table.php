<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_details`.
 */
class m180313_192415_create_user_details_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('user_details', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'address' => $this->string(),
            'phone' => $this->string(),
        ],$tableOptions);

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-user_details-user_id',
            'user_details',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `user_details`
        $this->dropForeignKey(
            'fk-user_details-user_id',
            'user_details'
        );
        $this->dropTable('user_details');
    }
}
