<?php

use yii\db\Migration;

/**
 * Handles the creation of table `filters_categories`.
 */
class m180405_090255_create_filters_categories_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('filters_categories', [
            'category_id' => $this->integer()->notNull(),
            'filter_id' => $this->integer()->notNull(),
        ],$tableOptions);

        $this->addForeignKey(
            'fk-filters_categories-category_id',
            'filters_categories',
            'category_id',
            'category',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-filters_categories-filter_id',
            'filters_categories',
            'filter_id',
            'filters',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-filters_categories-category_id',
            'filters_categories'
        );
        $this->dropForeignKey(
            'fk-filters_categories-filter_id',
            'filters_categories'
        );

        $this->dropTable('filters_categories');
    }
}
