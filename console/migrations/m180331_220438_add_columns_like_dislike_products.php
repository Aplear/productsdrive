<?php

use yii\db\Migration;

/**
 * Class m180331_220438_add_columns_like_dislike_products
 */
class m180331_220438_add_columns_like_dislike_products extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('products', 'like',$this->integer()->defaultValue(0)->after('recommended'));
        $this->addColumn('products', 'dislike',$this->integer()->defaultValue(0)->after('like'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180331_220438_add_columns_like_dislike_products cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180331_220438_add_columns_like_dislike_products cannot be reverted.\n";

        return false;
    }
    */
}
