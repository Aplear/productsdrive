<?php

use yii\db\Migration;

/**
 * Class m180411_190417_add_column_percentage_fee_products_table
 */
class m180411_190417_add_column_percentage_fee_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('products', 'percentage_fee', $this->float()->notNull()->after('price'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180411_190417_add_column_percentage_fee_products_table cannot be reverted.\n";

        return false;
    }
}
