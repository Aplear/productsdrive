<?php

use yii\db\Migration;

/**
 * Handles the creation of table `filter_values`.
 */
class m180405_090700_create_filter_values_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('filter_values', [
            'id' => $this->primaryKey(),
            'filter_id' => $this->integer(),
            'title' => $this->string(),
            'sort' => $this->integer()->defaultValue(0),
            'status' => $this->integer()->defaultValue(1)
        ],$tableOptions);

        $this->addForeignKey(
            'fk-filter_values-filter_id',
            'filter_values',
            'filter_id',
            'filters',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-filter_values-filter_id',
            'filter_values'
        );

        $this->dropTable('filter_values');
    }
}
