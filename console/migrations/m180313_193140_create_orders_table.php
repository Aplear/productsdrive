<?php

use yii\db\Migration;

/**
 * Handles the creation of table `orders`.
 */
class m180313_193140_create_orders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('orders', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->null(),
            'order_session_key' => $this->string(),
            'quantity' => $this->integer()->null(),
            'first_name' => $this->string()->null(),
            'last_name' => $this->string()->null(),
            'email' => $this->string()->null(),
            'status' => $this->integer(),
            'products_total_price' => $this->double(),
            'delivery_price' => $this->double(),

            'address' => $this->string(),
            'address_details' => $this->binary(),
            'house_number' => $this->string(),
            'apartment_number' => $this->string(),

            'addition_text' => $this->text(),

            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ],$tableOptions);

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-orders-user_id',
            'orders',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `orders`
        $this->dropForeignKey(
            'fk-orders-user_id',
            'orders'
        );
        $this->dropTable('orders');
    }
}
