<?php

use yii\db\Migration;

/**
 * Handles the creation of table `files`.
 */
class m180309_090259_create_files_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('files', [
            'id' => $this->primaryKey(),
            'module' => $this->string(),
            'module_id' => $this->integer()->notNull(),
            'link' => $this->string(),
            'path' => $this->string(),
            'alt' => $this->string()->null(),
            'order' => $this->integer()->defaultValue(0),
        ],$tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('files');
    }
}
