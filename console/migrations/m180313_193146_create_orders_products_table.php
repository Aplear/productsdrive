<?php

use yii\db\Migration;

/**
 * Handles the creation of table `orders_products`.
 */
class m180313_193146_create_orders_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('orders_products', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'product_id' => $this->integer(),
            'quantity' => $this->integer(),
            'price' => $this->double(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ],$tableOptions);

        // add foreign key for table `orders    `
        $this->addForeignKey(
            'fk-orders_products-order_id',
            'orders_products',
            'order_id',
            'orders',
            'id',
            'CASCADE'
        );

        // add foreign key for table `orders_products`
        $this->addForeignKey(
            'fk-orders_products-product_id',
            'orders_products',
            'product_id',
            'products',
            'id',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `orders`
        $this->dropForeignKey(
            'fk-orders_products-order_id',
            'orders_products'
        );
        // drops foreign key for table `orders_products`
        $this->dropForeignKey(
            'fk-orders_products-product_id',
            'orders_products'
        );

        $this->dropTable('orders_products');
    }
}
