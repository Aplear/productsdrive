<?php

use yii\db\Migration;

/**
 * Class m180331_224258_add_column_prone__orders
 */
class m180331_224258_add_column_prone__orders extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('orders', 'phone', $this->string()->null()->after('email'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180331_224258_add_column_prone__orders cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180331_224258_add_column_prone__orders cannot be reverted.\n";

        return false;
    }
    */
}
