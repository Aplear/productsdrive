<?php

use yii\db\Migration;

/**
 * Class m180320_090316_add_columns_products_table
 */
class m180320_090316_add_columns_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('products', 'main', $this->integer()->null()->after('status'));
        $this->addColumn('products', 'recommended', $this->integer()->null()->after('main'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180320_090316_add_columns_products_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180320_090316_add_columns_products_table cannot be reverted.\n";

        return false;
    }
    */
}
