<?php

use yii\db\Migration;

/**
 * Handles the creation of table `products`.
 */
class m180309_075509_create_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('products', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'code' => $this->string()->notNull(),
            'title' => $this->string()->notNull(),
            'alias' => $this->string()->unique(),
            'description' => $this->string()->notNull(),
            'keywords' => $this->string()->notNull(),
            'text' => $this->text(),
            'product_info' => $this->text(),

            'price' => $this->double(),
            'quantity' => $this->double(),
            'weight' => $this->float(),
            'denomination_of_weight_id' => $this->integer()->notNull(),
            'owner_id' =>$this->integer()->notNull(),

            'status' => $this->integer()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ],$tableOptions);

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-products-owner_id',
            'products',
            'owner_id',
            'user',
            'id',
            'CASCADE'
        );

        // add foreign key for table `denomination_of_weights`
        $this->addForeignKey(
            'fk-products-denomination_of_weight_id',
            'products',
            'denomination_of_weight_id',
            'denomination_of_weights',
            'id',
            'CASCADE'
        );

        // add foreign key for table `category`
        $this->addForeignKey(
            'fk-products-category_id',
            'products',
            'category_id',
            'category',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-products-owner_id',
            'products'
        );
        // drops foreign key for table `denomination_of_weights`
        $this->dropForeignKey(
            'fk-products-denomination_of_weight_id',
            'products'
        );
        // drops foreign key for table `category`
        $this->dropForeignKey(
            'fk-products-category_id',
            'products'
        );
        $this->dropTable('products');
    }
}
