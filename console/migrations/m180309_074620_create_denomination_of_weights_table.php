<?php

use yii\db\Migration;

/**
 * Handles the creation of table `denomination_of_weights`.
 */
class m180309_074620_create_denomination_of_weights_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('denomination_of_weights', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'abbreviation' => $this->string()->notNull()
        ],$tableOptions);

        $this->insert('denomination_of_weights',array(
            'title'=>'Миллилитры',
            'abbreviation' => 'мл.'
        ));
        $this->insert('denomination_of_weights',array(
            'title'=>'Литры',
            'abbreviation' => 'л.'
        ));
        $this->insert('denomination_of_weights',array(
            'title'=>'Миллиграмы',
            'abbreviation' => 'мг.'
        ));
        $this->insert('denomination_of_weights',array(
            'title'=>'Грамы',
            'abbreviation' => 'г.'
        ));
        $this->insert('denomination_of_weights',array(
            'title'=>'Килограмы',
            'abbreviation' => 'кг.'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('denomination_of_weights');
    }
}
