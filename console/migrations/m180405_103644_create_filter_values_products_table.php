<?php

use yii\db\Migration;

/**
 * Handles the creation of table `filter_values_products`.
 */
class m180405_103644_create_filter_values_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('filter_values_products', [
            'id' => $this->primaryKey(),
            'filter_value_id' => $this->integer(),
            'product_id' => $this->integer()
        ],$tableOptions);

        $this->addForeignKey(
            'fk-filter_values_products-filter_value_id',
            'filter_values_products',
            'filter_value_id',
            'filter_values',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-filter_values_products-product_id',
            'filter_values_products',
            'product_id',
            'products',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-filter_values_products-filter_value_id',
            'filter_values_products'
        );

        $this->dropForeignKey(
            'fk-filter_values_products-product_id',
            'filter_values_products'
        );

        $this->dropTable('filter_values_products');
    }
}
