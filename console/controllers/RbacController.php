<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    protected $auth;
    protected $client;
    protected $driver;
    protected $manager;
    protected $major_manager;
    protected $bookkeeper;
    protected $chief_editor;
    protected $admin;

    public function actionInit()
    {

        $this->auth = Yii::$app->authManager;
        $this->auth->removeAll();
        $this->roleInitialize();
        // ----------- USER && PROFILE -------------
        $this->setUserAccess();
        $this->setProfileAccess();
        // ----------- USER -------------
    }

    public function roleInitialize()
    {
        $this->client = $this->auth->createRole('client');
        $this->auth->add($this->client);

        $this->bookkeeper = $this->auth->createRole('bookkeeper');
        $this->auth->add($this->bookkeeper);

        $this->driver = $this->auth->createRole('driver');
        $this->auth->add($this->driver);

        $this->manager = $this->auth->createRole('manager');
        $this->auth->add($this->manager);

        $this->major_manager = $this->auth->createRole('major_manager');
        $this->auth->add($this->major_manager);

        $this->chief_editor = $this->auth->createRole('chief_editor');
        $this->auth->add($this->chief_editor);

        $this->admin = $this->auth->createRole('admin');
        $this->auth->add($this->admin);

        $this->admin = $this->auth->getRole('admin');
        $this->auth->addChild($this->admin, $this->chief_editor);

        $this->auth->addChild($this->major_manager, $this->manager);
        $this->auth->addChild($this->admin, $this->client);
        $this->auth->addChild($this->admin, $this->major_manager);
        $this->auth->addChild($this->admin, $this->driver);
        $this->auth->addChild($this->admin, $this->bookkeeper);
    }

    /**
     *
     */
    public function setUserAccess()
    {
        // add "createUser" permission
        $createUser = $this->auth->createPermission('createUser');
        $createUser->description = 'Create a user';
        $this->auth->add($createUser);

        // add "updateUser" permission
        $updateUser = $this->auth->createPermission('updateUser');
        $updateUser->description = 'Update user';
        $this->auth->add($updateUser);

        // add "deleteUser" permission
        $deleteUser = $this->auth->createPermission('deleteUser');
        $deleteUser->description = 'Delete user';
        $this->auth->add($deleteUser);

        // add "viewUser" permission
        $viewUser = $this->auth->createPermission('viewUser');
        $viewUser->description = 'View user';
        $this->auth->add($viewUser);

        // add "viewUsers" permission
        $viewUsers = $this->auth->createPermission('viewUsers');
        $viewUsers->description = 'View users';
        $this->auth->add($viewUsers);

        $this->auth->addChild($this->manager, $viewUsers);

        $this->auth->addChild($this->major_manager, $viewUser);

        $this->auth->addChild($this->admin, $createUser);
        $this->auth->addChild($this->admin, $updateUser);
        $this->auth->addChild($this->admin, $deleteUser);

        // Assign roles to users. 1 and 2 are IDs returned by IdentityInterface::getId()
        // usually implemented in your User model.
        $this->auth->assign($this->admin, 1);
    }


    public function setProfileAccess()
    {
        // add "create" permission
        $createProfile = $this->auth->createPermission('createProfile');
        $createProfile->description = 'Create a profile';
        $this->auth->add($createProfile);

        // add "update" permission
        $updateProfile = $this->auth->createPermission('updateProfile');
        $updateProfile->description = 'Update profile';
        $this->auth->add($updateProfile);

        // add "delete" permission
        $deleteProfile = $this->auth->createPermission('deleteProfile');
        $deleteProfile->description = 'Delete profile';
        $this->auth->add($deleteProfile);

        // add "view" permission
        $viewProfile = $this->auth->createPermission('viewProfile');
        $viewProfile->description = 'View profile';
        $this->auth->add($viewProfile);

        $this->auth->addChild($this->manager, $viewProfile);

        $this->auth->addChild($this->admin, $createProfile);
        $this->auth->addChild($this->admin, $updateProfile);
        $this->auth->addChild($this->admin, $deleteProfile);


    }


}