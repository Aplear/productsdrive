<?php

namespace backend\models;

use common\helper\TranslitHelper;
use Yii;
use common\models\User;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property int $category_id
 * @property string $code
 * @property string $title
 * @property string $alias
 * @property string $description
 * @property string $keywords
 * @property string $product_info
 * @property string $text
 * @property double $price
 * @property double $price_fee
 * @property double $percentage_fee
 * @property double $new_price
 * @property double $quantity
 * @property double $weight
 * @property int $denomination_of_weight_id
 * @property int $owner_id
 * @property int $status
 * @property int $main
 * @property int $recommended
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Category $category
 * @property DenominationOfWeights $denominationOfWeight
 * @property User $owner
 */
class Products extends \common\models\Products
{
    const STATUS_ACTIVE = 1;
    const STATUS_NOT_ACTIVE = 2;
    const STATUS_OVER = 3;
    const STATUS_DELETE = 4;
    const STATUS_PROMOTION = 5;

    /**
     * 3 MB
     */
    const MAX_FILE_SIZE = 1024 * 1024 * 5;

    public $status_array = [
        self::STATUS_ACTIVE => 'Активный',
        self::STATUS_NOT_ACTIVE => 'Не активный',
        self::STATUS_OVER => 'Закончился',
        self::STATUS_DELETE => 'Удален',
        self::STATUS_PROMOTION => 'Акционный',
    ];

    /**
     * @var $files[]
     */
    public $files;

    /**
     * @var $$filter[]
     */
    public $filter;

    public $percentage_fee_array = [
        0 => '0 %',
        1 => '1 %',
        3 => '3 %',
        5 => '5 %',
        7 => '7 %',
        10 => '10 %',
        13 => '13 %',
        15 => '15 %',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['price', 'category_id', 'title', 'description', 'keywords', 'denomination_of_weight_id', 'owner_id'], 'required'],
            [['main','recommended','category_id', 'denomination_of_weight_id', 'owner_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['text', 'product_info'], 'string'],
            [['price', 'price_fee', 'new_price', 'quantity', 'weight'], 'number'],
            ['percentage_fee', 'in', 'range' => [0, 1, 3, 5, 7, 10, 13, 15]],
            [['code', 'title', 'alias', 'description', 'keywords'], 'string', 'max' => 255],
            [['alias'], 'unique'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['denomination_of_weight_id'], 'exist', 'skipOnError' => true, 'targetClass' => DenominationOfWeights::className(), 'targetAttribute' => ['denomination_of_weight_id' => 'id']],
            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['owner_id' => 'id']],
            [['filter'], 'validateFilter'],
            [['files'], 'file',
                'skipOnEmpty' => true,
                'extensions' => 'djvu, png, jpg',
                'maxFiles' => 5,
                'maxSize' => self::MAX_FILE_SIZE,
                'tooBig' => 'Limit is 5 MB'],
            ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Категория',
            'code' => 'Код продукта',
            'title' => 'Title',
            'alias' => 'Alias',
            'description' => 'Description',
            'keywords' => 'Keywords',
            'text' => 'Text',
            'price' => 'Price производителя',
            'price_fee' => 'Цена с наценкой (грн)',
            'percentage_fee' => 'Процентная наценка (грн)',
            'new_price' => 'Акционная цена',
            'quantity' => 'Количество',
            'weight' => 'Вес',
            'denomination_of_weight_id' => 'Наминал веса',
            'owner_id' => 'Owner ID',
            'status' => 'Статус',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateFilter($attribute, $params)
    {

    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        $this->owner_id = Yii::$app->user->id;
        //$this->price_fee = ($this->price * $this->percentage_fee) / 100;
        return parent::beforeValidate(); // TODO: Change the autogenerated stub
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if(Yii::$app->controller->action->id === 'create') {
            $this->alias = $this->prepareAlias($this->title);
        } else if(Yii::$app->controller->action->id === 'update') {
            if($this->title !== $this->getOldAttribute('title')) {
                $this->alias = $this->prepareAlias($this->title);
            }
        }

        return parent::beforeSave($insert);
    }

    /**
     * @param $title
     * @return string
     * @internal param $title
     */
    public function prepareAlias($title)
    {
        $translitHelper = new TranslitHelper();
        $alias = strtolower($translitHelper->str2url($title));
        $productSame = Products::find()->where(['alias'=>$alias])->one();
        if(!is_null($productSame) || !empty($productSame)) {
            unset($translitHelper);
            return $this->prepareAlias($title);
        }
        return $alias;
    }

    /**
     * Product Code Generate Function
     * set 2(two) upper letters string
     * set 5(five) numbers int
     * check code unique
     * Set string (unique code)
     */
    public function generateProductCode()
    {
        //generate random values array of in and string items
        $num = range(0, 9);
        $alf = range('A', 'Z');

        shuffle($num);
        shuffle($alf);
        //get int code
        $code_array_num = array_slice($num, 0, 5);
        $code_num = implode("", $code_array_num);
        //get string code
        $code_array_alf = array_slice($alf, 0, 2);
        $code_alf = implode("", $code_array_alf);
        //concatenate two types od code
        $code = "#".$code_alf.$code_num;
        //if not set in db return code
        if(is_null(Products::findOne(['code' => $code]))) {
            return $code;
        }
        //against if not unique
        $this->generateCalculateCode();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDenominationOfWeight()
    {
        return $this->hasOne(DenominationOfWeights::className(), ['id' => 'denomination_of_weight_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilterValues()
    {
        return $this->hasMany(FilterValues::className(), ['id' => 'filter_value_id'])
            ->viaTable('filter_values_products', ['product_id' => 'id'], function ($query) {
                //$query->orderBy(['order' => SORT_DESC]);
            });
    }

    public function prepareFilter()
    {
        $filters = ArrayHelper::map($this->filterValues, 'id','filter_id' );

        foreach ($filters as $value => $filter_id) {

            $this->filter[$filter_id][] = $value;
        }
    }

    /**
     * @return array
     */
    public function setFiltersIdForSql()
    {
        $filtersArray = [];

        foreach ($this->filter as $filters) {

            foreach ($filters as $filter) {
                $filtersArray[$filter] = $filter;
            }
        }
        return $filtersArray;
    }
}
