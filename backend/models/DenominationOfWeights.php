<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "denomination_of_weights".
 *
 * @property int $id
 * @property string $title
 * @property string $abbreviation
 *
 * @property Products[] $products
 */
class DenominationOfWeights extends \common\models\DenominationOfWeights
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'denomination_of_weights';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'abbreviation'], 'required'],
            [['title', 'abbreviation'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'abbreviation' => 'Abbreviation',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Products::className(), ['denomination_of_weight_id' => 'id']);
    }
}
