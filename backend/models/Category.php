<?php

namespace backend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use common\helper\TranslitHelper;
/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property int $parent_id
 * @property string $name
 * @property string $alias
 * @property string $title
 * @property string $description
 * @property string $keywords
 * @property int $sort
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 */
class Category extends \common\models\Category
{
    const STATUS_DELETE = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'sort', 'status'], 'integer'],
            [['keywords'], 'required'],
            [['name', 'alias', 'title', 'description', 'keywords'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['alias'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'name' => 'Name',
            'alias' => 'Alias',
            'title' => 'Title',
            'description' => 'Description',
            'keywords' => 'Keywords',
            'sort' => 'Sort',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if(Yii::$app->controller->action->id === 'create') {
            $this->alias = $this->prepareAlias($this->name);
        } else if(Yii::$app->controller->action->id === 'update') {
           if($this->name !== $this->getOldAttribute('name')) {
               $this->alias = $this->prepareAlias($this->name);
           }
        }
        $this->title = $this->name;
        $this->description = $this->name;
        return parent::beforeSave($insert);
    }

    /**
     * @param $name
     * @return string
     * @internal param $title
     */
    public function prepareAlias($name)
    {
        $translitHelper = new TranslitHelper();
        $alias = strtolower($translitHelper->str2url($name));
        $categorySame = Category::find()->where(['alias'=>$alias])->one();
        if(!is_null($categorySame) || !empty($categorySame)) {
            unset($translitHelper);
            return $this->prepareAlias($name);
        }
        return $alias;
    }

    public static function getHierarchy() {
        $options = [];

        $parents = self::find()->where(['parent_id'=>null])
            ->andWhere(['<>','status',Category::STATUS_DELETE])
            ->all();

        foreach($parents as $id => $p) {
            $children = self::find()
                ->andWhere(['<>','status',Category::STATUS_DELETE])
                ->where("parent_id=:parent_id", [":parent_id"=>$p->id])
                ->all();
            $child_options = [];
            foreach($children as $child) {
                $child_options[$child->id] = $child->name;
            }
            $options[$p->name] = $child_options;
        }
        return $options;
    }

    /**
     * @param int $parent_id
     * @param string $exclude
     * @param string $space
     * @param string $categories
     * @return array|string
     */
    public function getCategoriesHierarchy($parent_id = 0, $exclude = '', $space = '', $categories='')
    {
        if($parent_id == 0){
            $space = '';
            $categories = array();
        }else{
            $space .='- ';
        }

        $model = Category::find()
            ->where(['parent_id' => $parent_id])
            ->andWhere(['<>','status',Category::STATUS_DELETE])
            ->all();
        if(!empty($model)){
            foreach ($model as $key) {
                if($key->id == $exclude) continue;
                $categories[] = array('id'=>$key->id, 'name'=>$space.$key->name);
                $categories = $this->getCategoriesHierarchy($key->id, $exclude, $space, $categories);
            }
        }

        return $categories;
    }
}
