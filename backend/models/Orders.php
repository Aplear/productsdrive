<?php

namespace backend\models;

use Yii;
use common\models\User;
/**
 * This is the model class for table "orders".
 *
 * @property int $id
 * @property int $user_id
 * @property int $status
 * @property double $products_total_price
 * @property double $delivery_price
 * @property string $address
 * @property string $district
 * @property string $apartment_number
 * @property string $addition_text
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User $user
 * @property OrdersProducts[] $ordersProducts
 */
class Orders extends \common\models\Orders
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['products_total_price', 'delivery_price'], 'number'],
            [['addition_text','district'], 'string'],
            [['created_at', 'updated_at'], 'required'],
            [['address', 'apartment_number'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'status' => 'Status',
            'products_total_price' => 'Products Price',
            'delivery_price' => 'Delivery Price',
            'address' => 'Address',
            'apartment_number' => 'Apartment Number',
            'addition_text' => 'Addition Text',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdersProducts()
    {
        return $this->hasMany(OrdersProducts::className(), ['order_id' => 'id']);
    }

    public function getPrice($id)
    {

    }
}
