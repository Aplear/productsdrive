<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "filter_values".
 *
 * @property int $id
 * @property int $filter_id
 * @property string $title
 * @property int $sort
 * @property int $status
 *
 * @property Category $category
 * @property Filters $filter
 */
class FilterValues extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'filter_values';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['filter_id', 'sort', 'status'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['filter_id'], 'exist', 'skipOnError' => true, 'targetClass' => Filters::className(), 'targetAttribute' => ['filter_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'filter_id' => 'Filter',
            'title' => 'Title',
            'sort' => 'Sort',
            'status' => 'Status',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilter()
    {
        return $this->hasOne(Filters::className(), ['id' => 'filter_id']);
    }
}
