<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "filter_values_products".
 *
 * @property int $id
 * @property int $filter_value_id
 * @property int $product_id
 *
 * @property FilterValues $filterValue
 * @property Products $product
 */
class FilterValuesProducts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'filter_values_products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['filter_value_id', 'product_id'], 'integer'],
            [['filter_value_id'], 'exist', 'skipOnError' => true, 'targetClass' => FilterValues::className(), 'targetAttribute' => ['filter_value_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'filter_value_id' => 'Filter Value ID',
            'product_id' => 'Product ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilterValue()
    {
        return $this->hasOne(FilterValues::className(), ['id' => 'filter_value_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }
}
