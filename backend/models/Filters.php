<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "filters".
 *
 * @property int $id
 * @property string $title
 * @property int $status
 *
 * @property FilterValues[] $filterValues
 * @property FiltersCategories[] $filtersCategories
 */
class Filters extends \yii\db\ActiveRecord
{
    /**
     * @var $categories_array
     */
    public $categories_array;

    /**
     * @var $selected_categories
     */
    public $selected_categories;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'filters';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['categories_array', 'each', 'rule' => ['integer']],
            [['status'], 'integer'],
            ['status', 'default', 'value' => 1],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilterValues()
    {
        return $this->hasMany(FilterValues::className(), ['filter_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiltersCategories()
    {
        return $this->hasMany(FiltersCategories::className(), ['filter_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiltersCategoriesArray()
    {
        return $this->hasMany(FiltersCategories::className(), ['filter_id' => 'id'])->asArray();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['id' => 'category_id'])
            ->viaTable('filters_categories', ['filter_id' => 'id'], function ($query) {
                //$query->orderBy(['order' => SORT_DESC]);
            });
    }

    public function prepareDropDownCategories($categories)
    {
        foreach ($categories as $category) {
            $this->selected_categories[$category['category_id']] = ['selected' => true];
        }
    }
}
