$(document).on('change','.status_change, .change-price-ajax',function (e) {
    e.stopPropagation();
    e.preventDefault();
    $('.preloader-overlay').show();
    var url = $(this).data('url')+$(this).val();
    $.ajax({
        type : "GET",
        url : url,
        //async:true,
        success : function (response) {
            $('.preloader-overlay').hide();
        },
        error : function (response) {
            $('.preloader-overlay').hide();
            return false;
        }
    });
});