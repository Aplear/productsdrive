$(document).on('change','#products-status',function () {
   if(parseInt($(this).val()) === 5){
       $('.new-price-products').removeClass('hidden');
   } else {
       $('.new-price-products').addClass('hidden');
   }
});

// Change price_fee
$(document).on('change','#products-percentage_fee',function () {
    var percentage = $(this).val();
    var price = $('#products-price').val();
    var price_fee = preparePriceFee(price, percentage);
    $('#products-price_fee').val(price_fee);
});

$(document).on('keyup','#products-price',function () {
    var price= $(this).val();
    var percentage  = $('#products-percentage_fee').val();
    var price_fee = preparePriceFee(price, percentage);
    $('#products-price_fee').val(price_fee);
});
//End Change price_fee

//function return price_fee
function preparePriceFee(price, percentage) {
    return ((parseFloat(price) * parseInt(percentage)) / 100)+parseFloat(price);
}