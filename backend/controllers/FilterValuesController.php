<?php

namespace backend\controllers;

use backend\models\Filters;
use Yii;
use backend\models\FilterValues;
use backend\models\search\FilterValuesSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FilterValuesController implements the CRUD actions for FilterValues model.
 */
class FilterValuesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],

                ],
            ],
        ];
    }

    /**
     * Lists all FilterValues models.
     * @param $filter_id
     * @return mixed
     */
    public function actionIndex($filter_id)
    {
        $searchModel = new FilterValuesSearch();
        $searchModel->filter_id = $filter_id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $filter = Filters::findOne($filter_id);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'filter' => $filter
        ]);
    }

    /**
     * Displays a single FilterValues model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FilterValues model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param $filter_id
     * @return mixed
     */
    public function actionCreate($filter_id)
    {
        $model = new FilterValues();
        $model->filter_id = $filter_id;
        $filter = Filters::findOne($filter_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'filter_id' => $filter_id]);
        }

        return $this->render('create', [
            'model' => $model,
            'filter' => $filter
        ]);
    }

    /**
     * Updates an existing FilterValues model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'filter_id' => $model->filter_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing FilterValues model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FilterValues model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FilterValues the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FilterValues::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
