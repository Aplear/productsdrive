<?php

namespace backend\controllers;

use common\models\Files;
use Yii;

use yii\base\Exception;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\FileHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\View;
use ZipArchive;

class FilesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['GET', 'POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],

                ],
            ],
        ];
    }

    /**
     * Deletes an existing Files model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        unlink($model->path);
        $model->delete();
        return true;
    }

    /**
     * @param null $type
     * @param int $id
     * @return $this
     */
    public function actionDownloadFiles($type=null, int $id)
    {
        $sourceFile = false;
        $modelFiles = new Files();
        //download files
        $pathToOrdersZip = Yii::getAlias('@webroot/downloads');
        $modelFiles->checkPath($pathToOrdersZip);
        $files = Files::find()
            ->where(['module_id'=>$id])
            ->andWhere(['module'=>$type])
            ->all();
        if(count($files) === 1) {
            $sourceFile = $files[0]->path;
        } else {

            $fileName = '_products.zip';
            $sourceFile = $pathToOrdersZip.'/'.$fileName;
            //archive all founded files
            $this->createZip($files,$sourceFile);
        }

        if($sourceFile !== false){
            Yii::$app->getResponse()->sendFile($sourceFile);
        }
    }



    /**
     * Finds the Files model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Files the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Files::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Method get all records from files and check have each existed files
     * if not delete records
     */
    public function actionDeleteRecordsWithoutFiles()
    {
        $models = Files::find()->all();
        foreach ($models as $model) {
            if(!is_file($model->path)) {
                $model->delete();
            }
        }
    }
}
