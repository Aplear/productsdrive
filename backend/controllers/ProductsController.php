<?php

namespace backend\controllers;

use backend\models\Filters;
use backend\models\FilterValues;
use common\models\Files;
use Yii;
use backend\models\Products;
use backend\models\search\ProductsSearch;
use yii\bootstrap\ActiveForm;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * ProductsController implements the CRUD actions for Products model.
 */
class ProductsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],

                ],
            ],
        ];
    }

    /**
     * Lists all Products models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['<>','status',Products::STATUS_DELETE]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Products model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Products model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Products();

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if(Yii::$app->request->post() && $model->load(Yii::$app->request->post())) {
                return ActiveForm::validate($model);
            }
        }

        if($model->load(Yii::$app->request->post()) && $model->validate()) {
            //start transaction
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                $model->code = $model->generateProductCode();
                $model->save();
                $files = new Files();
                $files->uploadImage($model);
                //close trunsation
                $transaction->commit();
            } catch (\Exception $e) {
                Yii::$app->session->setFlash('error', 'System error. line '.__LINE__);
                Yii::error('System error. line '.__LINE__);
                $transaction->rollback();
            }
            return $this->redirect('index');
        }

        return $this->render('create', [
            'model' => $model,
            'filesArray' => [],
            'filesConfigArray' => []
        ]);
    }

    /**
     * Updates an existing Products model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        //get files
        $model->files = Files::find()->where([
            'module'=>Yii::$app->controller->id,
            'module_id'=>$model->id
        ])->asArray()->all();

        $filesModel = new Files();
        $filesArray = $filesModel->prepareKartikInitialPreview($model->files);

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if(Yii::$app->request->post() && $model->load(Yii::$app->request->post())) {
                return ActiveForm::validate($model);
            }
        }
        if($model->load(Yii::$app->request->post()) && $model->validate()) {
            //start transaction
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                $model->save();
                $files = new Files();
                $files->uploadImage($model);
                //close trunsation
                $transaction->commit();
            } catch (\Exception $e) {
                Yii::$app->session->setFlash('error', 'System error. line '.__LINE__);
                Yii::error('System error. line '.__LINE__);
                $transaction->rollback();
            }

            return $this->redirect('index');
        }

        return $this->render('update', [
            'model' => $model,
            'filesArray' => is_null(array_values($filesArray['files']))?[]:array_values($filesArray['files']),
            'filesConfigArray' => is_null(array_values($filesArray['filesConfig']))?[]:array_values($filesArray['filesConfig']),
        ]);
    }

    /**
     * Updates an existing Products model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param $product_id
     * @param $category_id
     * @return mixed
     * @internal param int $id
     */
    public function actionFilters($product_id, $category_id)
    {
        $product = $this->findModel($product_id);
        $filters = ArrayHelper::map(Filters::find()
            ->joinWith('categories')
            ->andFilterWhere(['category.id'=>$category_id])
            ->all(), 'id','title');

        $filterValues = ArrayHelper::map(FilterValues::find()
        ->where(['in','filter_id',array_keys($filters)])
        ->all(), 'id', 'title', 'filter_id');

        if($product->load(Yii::$app->request->post()) && $product->validate()) {
            $filtersArray = $product->setFiltersIdForSql();
            //start transaction
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                //Get all filter values models we choose
                $filterValues = FilterValues::findAll($filtersArray);
                $product->unlinkAll('filterValues', true);
                //Check models $categories
                if(!empty($filterValues)) {
                    foreach ($filterValues as $filterValue) {
                        $product->link('filterValues', $filterValue);
                    }
                }
                //close trunsation
                $transaction->commit();

            } catch (\Exception $e) {
                Yii::$app->session->setFlash('error', 'System error. line '.__LINE__);
                Yii::error('System error. line '.__LINE__);
                $transaction->rollback();
            }
            return $this->redirect('index');
        }

        //$product->filter =ArrayHelper::getColumn($product->filterValues, 'id');
        $product->prepareFilter();
        return $this->render('_form_filters', [
            'product' => $product,
            'filterValues' => $filterValues,
            'filters' => $filters
        ]);
    }

    /**
     * Deletes an existing Products model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->status = Products::STATUS_DELETE;
        $model->save();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Products model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Products the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Products::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Export all XeroContacts models.
     * @return mixed
     */
    public function actionExport()
    {
        $searchModel = new ProductsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy(['postal_address_country'=>SORT_ASC]);
        $dataProvider->pagination = false;
        return $this->render('export', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @param $status
     * @return bool
     */
    public function actionChangeStatus($id, $status)
    {
        if(Yii::$app->request->isAjax) {
            $model = $this->findModel($id);
            $model->status = $status;
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $model->save(false);
        }
        return false;
    }

    /**
     * @param $id
     * @param $status
     * @return bool
     */
    public function actionChangePrice($id, $price)
    {
        if(Yii::$app->request->isAjax) {
            $model = $this->findModel($id);
            $model->price = $price;
            $model->price_fee = (($price * $model->percentage_fee) / 100) + $price;
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $model->save(false);
        }
        return false;
    }
}
