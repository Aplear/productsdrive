<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \backend\models\Category;
use \backend\models\DenominationOfWeights;
use kartik\file\FileInput;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model backend\models\Products */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="products-form" style="display: inline-grid;">

    <?php $form = ActiveForm::begin([
        'id' => 'productFormSave',
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'fieldConfig' => [
            'options' => [],
            'template' => '{label}{input}{error}',
            'inputOptions' => [
                'class'=>'validate form-control',
            ]
        ],
        'options' => [
            //'class' => 'form-inline',
            'enctype' => 'multipart/form-data'
        ],
    ]); ?>

    <div class="col-md-12">
        <?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(Category::find()->all(), 'id', 'name')) ?>
    </div>

    <div class="col-md-12">
        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-md-12">
        <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-md-12">
        <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-md-12">
        <?= $form->field($model, 'keywords')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-md-12">
        <?= $form->field($model, 'text')->widget(CKEditor::className(), [
            'options' => ['rows' => 6],
            'preset' => 'basic'
        ]) ?>
    </div>

    <div class="col-md-12">
        <?= $form->field($model, 'product_info')->widget(CKEditor::className(), [
            'options' => ['rows' => 6],
            'preset' => 'basic'
        ]) ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'price')->textInput() ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'price_fee')->textInput() ?>
    </div>

    <?php
        if($model->status === \backend\models\Products::STATUS_PROMOTION) {
            $class = '';
        } else {
            $class = 'hidden';
        }
    ?>
    <div class="col-md-12">
        <?= $form->field($model,'percentage_fee')->dropDownList($model->percentage_fee_array)?>
    </div>

    <div class="col-md-12">
        <div class="<?=$class?> new-price-products">
            <?= $form->field($model, 'new_price')->textInput() ?>
        </div>
    </div>

    <div class="col-md-12">
        <?= $form->field($model, 'quantity')->textInput() ?>
    </div>

    <div class="col-md-12">
        <?= $form->field($model, 'weight')->textInput() ?>
    </div>

    <div class="col-md-12">
        <?= $form->field($model, 'denomination_of_weight_id')->dropDownList(ArrayHelper::map(DenominationOfWeights::find()->all(), 'id', 'title')) ?>
    </div>

    <div class="col-md-12">
        <?=$form->field($model, 'files[]')->widget(FileInput::classname(), [
            'options' => ['multiple'=>true,'accept' => '*'],
            'pluginOptions' => [
                'initialPreview'=>$filesArray,
                'initialPreviewConfig' => $filesConfigArray,
                'initialPreviewAsData'=>true,
                'initialPreviewFileType' => 'image',
                'uploadAsync' => false,
                'showUpload' => false,
                'browseClass' => 'btn btn-primary btn-block',
                'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                'browseLabel' =>  'Select File',
                'overwriteInitial'=>false,
            ],
        ])?>
    </div>

    <div class="col-md-12">
        <?= $form->field($model, 'status')->dropDownList($model->status_array) ?>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
    $this->registerJsFile("@web/js/products/form.js",[
        'position' => \yii\web\View::POS_END,
        'defer' => true
    ]);
?>