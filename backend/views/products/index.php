<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use \yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\Pjax;

use \backend\models\Category;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Продукты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-index">

    <div class="preloader-overlay" style="display: none;">
        <div class="preloader-loader" style="position: absolute;"></div>
    </div>

    <h1><?= Html::encode($this->title) ?></h1>
    <?php //Pjax::begin(); ?>
    <?php //Pjax::end(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
    echo ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => [],
            'fontAwesome' => true,
            'dropdownOptions' => [
                'label' => 'Export All',
                'class' => 'btn btn-default'
            ]
        ]) . "<hr>\n";
    ?>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-striped table-bordered'
        ],
        'options'=> [
            'class' => 'table-responsive',
        ],
        'columns' => [
            [
                'header' => 'Category',
                'attribute' => 'category_id',
                'value' => function($model) {
                    return Category::findOne($model->category_id)->name;
                },
                'filter'=> ArrayHelper::map(Category::find()->all(),'id','title'),
            ],
            'code',
            'title',
            'alias',
            //'description',
            //'keywords',
            //'text:ntext',
            [
                'header'=>'Цена',
                'attribute'=>'price',
                'value' => function ($model,$index) {
                    return HTML::textInput('price',$model->price,[
                        'class' => 'change-price-ajax',
                        'data-url' => Url::toRoute('/products/change-price?id='.$model->id.'&price='),
                    ]);
                },
                'format' => 'raw',
            ],
            //'quantity',
            //'weight',
            //'denomination_of_weight_id',
            //'owner_id',
            [
                'attribute'=>'status',
                'value' => function ($model,$index) {

                    return HTML::activeDropDownList($model, 'status', $model->status_array, [
                        'class' => 'form-control status_change',
                        'data-url' => Url::toRoute('/products/change-status?id='.$model->id.'&status='),
                        //'options' => $disabledStatusArray
                    ]);
                },
                'format' => 'raw',
                'filter'=> $searchModel->status_array,
            ],
            //'created_at',
            //'updated_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} <br /> {update} <br /> {delete} <br /> {filters}',
                'buttons' => [

                    'filters' => function ($url,$model) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-filter"></span>',
                            Url::to(['filters', 'product_id'=>$model->id, 'category_id' => $model->category_id]),
                            [ 'id'=>'modal-btn-filters', 'title' => 'Filters', 'aria-label'=>'Filters', 'data-pjax' => 0]
                        );
                    },
                ],
            ],
        ],
    ]); ?>

</div>

<?php
$this->registerJsFile("@web/js/products/index.js",[
    'position' => \yii\web\View::POS_END,
    'defer' => true
]);
?>