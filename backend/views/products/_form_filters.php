<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $product backend\models\Products */
/* @var $filterValues backend\models\FilterValues */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="filters-products-form">

    <?php $form = ActiveForm::begin([
        'id' => 'productFormSave',
        'enableAjaxValidation' => false,
        'enableClientValidation' => false,
        'fieldConfig' => [
            'options' => [],
            'template' => '{label}{input}{error}',
            'inputOptions' => [
                'class'=>'validate form-control',
            ]
        ],
        'options' => [
            'class' => 'form-vertical',
            //'enctype' => 'multipart/form-data'
        ],
    ]); ?>

    <?php foreach ($filterValues as $filter_id => $filterValue):?>
        <ul>
            <li>
                <label><?=$filters[$filter_id]?></label>
            </li>
            <ul>
                <?= $form->field($product, "filter[$filter_id][]")->checkboxList($filterValue,['separator' => '<br>'])?>
            </ul>
        </ul>
    <?php endforeach;?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
