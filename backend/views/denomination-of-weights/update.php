<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\DenominationOfWeights */

$this->title = 'Update Denomination Of Weights: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Denomination Of Weights', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="denomination-of-weights-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
