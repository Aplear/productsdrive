<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\DenominationOfWeights */

$this->title = 'Create Denomination Of Weights';
$this->params['breadcrumbs'][] = ['label' => 'Denomination Of Weights', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="denomination-of-weights-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
