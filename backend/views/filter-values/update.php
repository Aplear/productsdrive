<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\FilterValues */

$this->title = 'Update Filter Values: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Filters', 'url' => ['index', 'filter_id'=>$model->filter_id]];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="filter-values-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
