<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\FilterValuesSearch */
/* @var $filter backend\models\Filters */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $filter->title.' Filter Values ';
$this->params['breadcrumbs'][] = ['label' => 'Filters', 'url' => ['/filters']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="filter-values-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Filter Values', ['create', 'filter_id'=>$filter->id], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'filter_id',
            'title',
            'sort',
            //'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
