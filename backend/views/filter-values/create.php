<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\FilterValues */
/* @var $filter backend\models\Filters */

$this->title = 'Create Filter Values For '. $filter->title;
$this->params['breadcrumbs'][] = ['label' => 'Filters', 'url' => ['/filters']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="filter-values-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
