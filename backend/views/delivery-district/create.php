<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\DeliveryDistrict */

$this->title = 'Create Delivery District';
$this->params['breadcrumbs'][] = ['label' => 'Delivery Districts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="delivery-district-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
