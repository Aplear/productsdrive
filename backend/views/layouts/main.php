<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use \yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<nav class="navbar navbar-default navbar-static-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?=Url::to('/admin')?>">
                <?=Yii::$app->name?>
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <form class="navbar-form navbar-left" method="GET" role="search">
                <div class="form-group">
                    <input type="text" name="q" class="form-control" placeholder="Search">
                </div>
                <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
            </form>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="<?=Yii::$app->urlManagerFrontEnd->hostInfo?>" target="_blank">Visit Site</a></li>
                <?php if (!Yii::$app->user->isGuest):?>
                    <li class="dropdown ">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <?=Yii::$app->user->identity->username?>
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li class="dropdown-header">SETTINGS</li>
                            <!--<li class=""><a href="#">Other Link</a></li>
                            <li class=""><a href="#">Other Link</a></li>
                            <li class=""><a href="#">Other Link</a></li>
                            <li class="divider"></li>-->
                            <li>
                                <?=Html::beginForm(['/site/logout'], 'post')
                                . Html::submitButton(
                                    'Logout',
                                    ['class' => 'btn btn-link logout']
                                )
                                . Html::endForm()?>
                            </li>
                        </ul>
                    </li>
                <?php endif;?>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<div class="container-fluid main-container">

    <div class="col-md-2 sidebar">
        <ul class="nav nav-pills nav-stacked">
            <li class="active"><a href="/">Dashboard</a></li>
            <li><a href="<?=Url::to(['/orders'])?>">Заказы</a></li>
            <li><a href="<?=Url::to(['/category'])?>">Категории</a></li>
            <li><a href="<?=Url::to(['/pages'])?>">Страницы</a></li>
            <li><a href="<?=Url::to(['/products'])?>">Продукты</a></li>
            <li><a href="<?=Url::to(['/filters'])?>">Фильтры</a></li>
            <li><a href="<?=Url::to(['/denomination-of-weights'])?>">Наминалы весов</a></li>
            <li><a href="<?=Url::to(['/delivery-district'])?>">Оплата доставки</a></li>
        </ul>
    </div>

    <div class="col-md-10 content">
        <div class="">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>
        </div>

        <div class="panel panel-default" style="padding: 5px;">
            <?= $content ?>
        </div>
    </div>
    <footer class="pull-right">
        <p class="col-md-12">
        <hr class="divider" />
        Copyright &COPY; 2018 <a href="">ProductsDrive</a>
        </p>
    </footer>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
