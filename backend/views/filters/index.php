<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\FiltersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Filters';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="filters-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Filters', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'title',
            'status',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} <br /> {update} <br /> {delete} <br /> {filter-values}',
                'buttons' => [
                    'update' => function ($url,$model) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-edit"></span>',
                            $url,
                            [ 'id'=>'modal-btn-update', 'title' => 'Update', 'aria-label'=>'Update', 'data-pjax' => 0]
                        );
                    },
                    'filter-values' => function ($url,$model) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-filter"></span>',
                            Url::to(['/filter-values/index', 'filter_id'=>$model->id]),
                            [ 'id'=>'modal-btn-filtere', 'title' => 'Filter values', 'aria-label'=>'Filter values', 'data-pjax' => 0]
                        );
                    },
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
