<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\helpers\ArrayHelper;
use \backend\models\Category;

/* @var $this yii\web\View */
/* @var $model backend\models\Filters */
/* @var $form yii\widgets\ActiveForm */

$category = new Category();
$categories = $category->getCategoriesHierarchy( null);
$items = ArrayHelper::map($categories, 'id', 'name', 'parent_id');

?>

<div class="filters-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'categories_array[]')->dropDownList($items,[
            'multiple'=>'multiple',
            'size' => 20,
            'options' => $model->selected_categories
            //'class'=>'chosen-select input-md required',
         ] )?>

    <?= $form->field($model, 'status')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
