<?php

namespace common\models;
use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\UploadedFile;
use function GuzzleHttp\Psr7\mimetype_from_filename;

/**
 * This is the model class for table "files".
 *
 * @property integer $id
 * @property string $module
 * @property string $module_id
 * @property string $link
 * @property string $path
 * @property string $alt
 * @property integer $order
 */
class Files extends \yii\db\ActiveRecord
{
    private $entity;
    private $webPath;
    public $files;

    public $image;
    public $image_type;

    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->entity = Yii::$app->controller->id;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'files';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order', 'module_id'], 'integer'],
            [['module', 'link', 'path', 'alt'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'module' => Yii::t('app', 'Module'),
            'link' => Yii::t('app', 'Link'),
            'path' => Yii::t('app', 'Path'),
            'alt' => Yii::t('app', 'Alt'),
            'order' => Yii::t('app', 'Order'),
        ];
    }

    /**
     * Method check server file path
     */
    public function checkPath($path)
    {
        if (!is_dir($path)) {
            FileHelper::createDirectory($path);
        }
        return $path;
    }

    /**
     * @return bool
     */
    public function uploadFile($model)
    {
        $this->checkPath(Yii::getAlias('@frontend/web/uploads/' . $this->entity));
        $serverPath = Yii::getAlias('@frontend/web/uploads/' . $this->entity . '/' . date('d-m-Y', time()));
        $webPath = Yii::getAlias('@web/uploads/' . $this->entity . '/' . date('d-m-Y', time()));
        $this->files = UploadedFile::getInstances($model, 'files');

        if ($this->validate()) {
            foreach ($this->files as $file) {
                //get file module
                $filesModule = new Files();

                //prepare file name
                $fileName = random_int(1, 999) . '_' . time() . '.' . $file->extension;
                //prepare file server path
                $filePath = $this->checkPath($serverPath) . '/' . $fileName;
                //prepare file web path
                $this->webPath = $webPath . '/' . $fileName;
                //if non set errors
                if (!$file->hasError) {
                    //upload file
                    $file->saveAs($filePath);
                    //prepare data for file module
                    $filesModule->module = $this->entity;
                    $filesModule->module_id = $model->id;
                    $filesModule->path = $filePath;
                    $filesModule->link = $this->webPath;
                    $filesModule->alt = $file->baseName . '.' . $file->extension;
                    $filesModule->save();
                }
            }
            return true;
        } else {
            return false;
        }
    }

    public function uploadImage($model)
    {
        $this->checkPath(Yii::getAlias('@frontend/web/uploads/' . $this->entity));
        $serverPath = Yii::getAlias('@frontend/web/uploads/' . $this->entity . '/' . date('d-m-Y', time()));
        $webPath = Yii::getAlias('/uploads/' . $this->entity . '/' . date('d-m-Y', time()));
        $this->files = UploadedFile::getInstances($model, 'files');

        if ($this->validate()) {
            foreach ($this->files as $file) {
                //get file module
                $filesModule = new Files();

                //prepare file name
                $fileName = random_int(1, 999) . '_' . time() . '.' . $file->extension;
                //prepare file server path
                $filePath = $this->checkPath($serverPath) . '/' . $fileName;
                //prepare file web path
                $this->webPath = $webPath . '/' . $fileName;
                //if non set errors
                if (!$file->hasError) {
                    //upload file
                    $file->saveAs($filePath);
                    //load for resize
                    $this->loadFile($filePath);
                    if ($this->getWidth() < $this->getHeight()) {
                        $this->resizeToHeight(700);
                    } else if($this->getWidth() > $this->getHeight()) {
                        $this->resizeToWidth(700);
                    } else {
                        $this->resize(700,700);
                    }
                    $this->saveImage($filePath);

                    //prepare data for file module
                    $filesModule->module = $this->entity;
                    $filesModule->module_id = $model->id;
                    $filesModule->path = $filePath;
                    $filesModule->link = $this->webPath;
                    $filesModule->alt = $file->baseName . '.' . $file->extension;
                    $filesModule->save();
                }
            }
            return true;
        } else {
            return false;
        }

    }

    /**
     * @param $file
     * @return mixed
     */
    public function getTypeInitialPreviewConfigByFile($file)
    {
        $path_parts = pathinfo($file);

        $mime_types = [
            'docx' => 'office',
            'xls' => 'office',
            'xlsx' => 'office',
            'xlsm' => 'office',
            'doc' => 'office',
            'pdf' => 'pdf',
            'txt' => 'text',
            'text' => 'text',
            'jpg' => 'image',
            'png' => 'image'
        ];
        return $mime_types[$path_parts['extension']];
    }

    /**
     * @param $files
     * @return array
     */
    public function prepareKartikInitialPreview($files)
    {
        $filesArray = [];
        $filesConfigArray = [];
        if(!empty($files)) {
            foreach ($files as $file) {
                if(is_file($file['path'])) {
                    $type = $this->getTypeInitialPreviewConfigByFile($file['path']);
                    $fileMimeType = mimetype_from_filename($file['path']);
                    $filesArray[] = Yii::$app->urlManagerFrontEnd->hostInfo.$file['link'];
                    $filesConfigArray[] = [
                        'caption' => $file['alt'],
                        'size' => filesize($file['path']),
                        'url' => Url::to(['/files/delete?id='.$file['id']]),
                        'filetype' => $fileMimeType,
                        'type' => $type
                    ];
                }
            }
        }

        return [
            'files' => $filesArray,
            'filesConfig' => $filesConfigArray,
        ];
    }

    function loadFile($filename) {
        $image_info = getimagesize($filename);
        $this->image_type = $image_info[2];
        if( $this->image_type == IMAGETYPE_JPEG ) {
            $this->image = imagecreatefromjpeg($filename);
        } elseif( $this->image_type == IMAGETYPE_GIF ) {
            $this->image = imagecreatefromgif($filename);
        } elseif( $this->image_type == IMAGETYPE_PNG ) {
            $this->image = imagecreatefrompng($filename);
        }
    }

    function getWidth() {
        return imagesx($this->image);
    }
    function getHeight() {
        return imagesy($this->image);
    }
    function resizeToHeight($height) {
        $ratio = $height / $this->getHeight();
        $width = $this->getWidth() * $ratio;
        $this->resize($width,$height);
    }
    function resizeToWidth($width) {
        $ratio = $width / $this->getWidth();
        $height = $this->getheight() * $ratio;
        $this->resize($width,$height);
    }
    function scale($scale) {
        $width = $this->getWidth() * $scale/100;
        $height = $this->getheight() * $scale/100;
        $this->resize($width,$height);
    }
    function resize($width,$height) {
        $new_image = imagecreatetruecolor($width, $height);
        imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
        $this->image = $new_image;
    }
    function saveImage($filename, $image_type=IMAGETYPE_JPEG, $compression=75, $permissions=null) {
        if( $image_type == IMAGETYPE_JPEG ) {
            imagejpeg($this->image,$filename,$compression);
        } elseif( $image_type == IMAGETYPE_GIF ) {
            imagegif($this->image,$filename);
        } elseif( $image_type == IMAGETYPE_PNG ) {
            imagepng($this->image,$filename);
        }
        if( $permissions != null) {
            chmod($filename,$permissions);
        }
    }
}
