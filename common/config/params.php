<?php
return [
    'adminEmail' => 'no-reply@products-drive.zp.ua',
    'supportEmail' => 'no-reply@products-drive.zp.ua',
    'secretKeyExpire' => 60 * 60,
    'user.passwordResetTokenExpire' => 3600,
    'emailActivation' => true //email activation
];
