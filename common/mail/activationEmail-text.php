<?php
/* @var $this yii\web\View */
/* @var $user frontend\models\User */
$activateLink = Yii::$app->urlManager->createAbsoluteUrl([
    '/site/activate-account',
    'key' => $user->secret_key
])
?>
    Hello <?= $user->username ?>,

    Follow the link below to activate your account:

<?= $activateLink ?>