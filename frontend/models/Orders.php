<?php
namespace frontend\models;

use Yii;
use common\models\User;
use \frontend\models\Products;

/**
 * This is the model class for table "orders".
 *
 * @property int $id
 * @property int $user_id
 * @property string $order_session_key
 * @property int $quantity
 * @property int $status
 * @property double $products_total_price
 * @property double $delivery_price
 * @property string $address
 * @property string $district
 * @property string $apartment_number
 * @property string $addition_text
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User $user
 * @property OrdersProducts[] $ordersProducts
 */
class Orders extends \common\models\Orders
{
    const STATUS_ACTIVATED = 1;
    const STATUS_CHECKIN = 2;
    const STATUS_CHECKOUT = 3;
    const STATUS_WAITING_CONFIRM = 4;
    const STATUS_CONFIRMED = 5;
    const STATUS_LOADING = 6;
    const STATUS_DELIVERD = 7;
    const STATUS_PRE_PAID = 8;
    const STATUS_PAID = 9;
    const STATUS_COMPLETED = 10;
    const STATUS_IN_ARCHIVE = 11;
    const STATUS_DELETED = 12;

    const DEFAULT_DELIVERY_PRICE = 66;

    const SCENARIO_CHECKOUT = 'checkout';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'status', 'created_at', 'updated_at'], 'integer'],

            [['first_name', 'last_name', 'email', 'address', 'phone'], 'required', 'on' => self::SCENARIO_CHECKOUT],
            ['email', 'email'],
            [['products_total_price', 'delivery_price'], 'number'],
            [['addition_text','order_session_key', 'phone'], 'string'],
            [['address', 'district', 'apartment_number'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'status' => 'Status',
            'products_total_price' => 'Products Price',
            'delivery_price' => 'Delivery Price',
            'address' => 'Address',
            'district' => 'District',
            'apartment_number' => 'Apartment Number',
            'addition_text' => 'Addition Text',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        $this->user_id = isset(Yii::$app->user->id)?Yii::$app->user->id:null;
        return parent::beforeSave($insert);
    }

    /**
     * @return mixed|string
     */
    public function generateOrderSessionKey()
    {
        $this->order_session_key = Yii::$app->security->generateRandomString();
        $duplicates = Orders::find()->where(['order_session_key' => $this->order_session_key])->all();
        if(!empty($duplicates)) {
            $this->generateOrderSessionKey();
        }
        $session = Yii::$app->session;
        $session->set('order_session_key', $this->order_session_key);
        return $this->order_session_key;
    }

    /**
     * Get order_session_key from user session
     * @return mixed
     */
    public function getOrderSessionKey()
    {
        $session = Yii::$app->session;
        return $session->get('order_session_key');
    }

    /**
     * @param null $order_session_key
     * @return array|\yii\db\ActiveRecord
     */
    public function getOrderBySessionKey($order_session_key=null)
    {
        if(is_null($order_session_key)){
            $order_session_key = $this->getOrderSessionKey();
        }

        return self::find()
            ->where(['order_session_key'=>$order_session_key])
            ->andWhere(['status'=>Orders::STATUS_ACTIVATED])
            ->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['product_id' => 'id']);
    }


}
