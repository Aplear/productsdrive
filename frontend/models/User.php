<?php
namespace frontend\models;

use common\models\UserDetails;
use Yii;
use yii\behaviors\TimestampBehavior;
use common\models\User as CommonUser;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 * @property string $secret_key
 *
 * @property object Auth[] $auth
 */
class User extends CommonUser
{
    const STATUS_DELETED = 0;
    const STATUS_NOT_ACTIVE = 1;
    const STATUS_ACTIVE = 10;

    public $role = self::ROLE_CLIENT_TITLE;
    public $password = null;


    const ROLE_ADMIN_TITLE = 'admin';
    const ROLE_DRIVER_TITLE = 'driver';
    const ROLE_MANAGER_TITLE = 'manager';
    const ROLE_BOOKKEEPER_TITLE = 'bookkeeper';
    const ROLE_CHIEFEDITOR_TITLE = 'chief_editor';
    const ROLE_CLIENT_TITLE = 'client';

    public $role_array = [
        'client' => 'Client',
        'driver' => 'Driver',
        'manager' => 'Manager',
        'major_manager' => 'Manager major',
        'bookkeeper' => 'Bookkeeper',
        'chief_editor' => 'Chief Editor',
        'admin' => 'Administraor',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\app\common\models\User', 'message' => 'This username has already been taken.', 'on' => 'create'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['role', 'string'],
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => 'app\common\models\User', 'message' => 'This email address has already been taken.', 'on' => 'create'],
            ['status', 'default', 'value' => CommonUser::STATUS_ACTIVE, 'on' => 'default'],
            ['status', 'in', 'range' =>[
                CommonUser::STATUS_NOT_ACTIVE,
                CommonUser::STATUS_ACTIVE
            ]],
            ['status', 'default', 'value' => CommonUser::STATUS_NOT_ACTIVE, 'on' => 'emailActivation'],
            ['secret_key', 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Create user.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function createUser()
    {
        if (!$this->validate()) {
            return null;
        }
        $user = new CommonUser();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->status = $this->status;
        if(isset($this->password)) {
            $user->setPassword($this->password);
        }
        $user->generateAuthKey();
        if($this->scenario === 'emailActivation') {
            $user->generateSecretKey();
        }
        if($user->save(false)) {
            $profile = new UserDetails();
            $profile->user_id = $user->id;
            $profile->save();

            // add the role for new user
            $auth = \Yii::$app->authManager;
            $authorRole = $auth->getRole($this->role);
            $auth->assign($authorRole, $user->getId());

            return $user;
        }
        return null;
    }

    /**
     * Send email with activation link
     * @param $user Object
     * @param $set_password boolean
     *
     * @return boolean
     */
    public function sendActivationEmail($user, $set_password=false)
    {
        return Yii::$app->mailer->compose(['html' => 'activationEmail-html', 'text' => 'activationEmail-text'], ['user' => $user])
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name.' (robot sent).'])
            ->setTo($this->email)
            ->setSubject('Activation for '.Yii::$app->name)
            ->send();
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserDetails()
    {
        return $this->hasOne(UserDetails::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuths()
    {
        return $this->hasMany(Auth::className(), ['user_id' => 'id']);
    }

}
