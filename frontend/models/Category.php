<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property int $parent_id
 * @property string $name
 * @property string $alias
 * @property string $title
 * @property string $description
 * @property string $keywords
 * @property int $sort
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Products[] $products
 */
class Category extends \common\models\Category
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'sort', 'status', 'created_at', 'updated_at'], 'integer'],
            [['title', 'description', 'keywords', 'created_at', 'updated_at'], 'required'],
            [['name', 'alias', 'title', 'description', 'keywords'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['alias'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'name' => 'Name',
            'alias' => 'Alias',
            'title' => 'Title',
            'description' => 'Description',
            'keywords' => 'Keywords',
            'sort' => 'Sort',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Products::className(), ['category_id' => 'id']);
    }

    /**
     * @param $id
     * @param array $catArray
     * @return array
     */
    public function getSubCategoriesId($id, $catArray=[])
    {
        // loop through category ids and find all child categories until there are no more
        $cat = Category::find()->select(['id', 'name'])->where(['parent_id'=>$id])->asArray()->all();

        if($cat > 0)
        {
            foreach($cat as $subcat){
                $catArray[$subcat['id']]= $subcat['name'];
                return $this->getSubCategoriesId($subcat['id'],$catArray);
            }
        }

        return $catArray;
    }
}
