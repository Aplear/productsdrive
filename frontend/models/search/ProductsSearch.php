<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Products;

/**
 * ProductsSearch represents the model behind the search form of `common\models\Products`.
 */
class ProductsSearch extends Products
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'denomination_of_weight_id', 'owner_id', 'status', 'main', 'recommended', 'created_at', 'updated_at'], 'integer'],
            [['code', 'title', 'alias', 'description', 'keywords', 'text'], 'safe'],
            [['price', 'new_price', 'quantity', 'weight'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public function search($params)
    {
        $query = Products::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'price' => $this->price,
            'new_price' => $this->new_price,
            'quantity' => $this->quantity,
            'weight' => $this->weight,
            'denomination_of_weight_id' => $this->denomination_of_weight_id,
            'owner_id' => $this->owner_id,
            'status' => $this->status,
            'main' => $this->main,
            'recommended' => $this->recommended,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'alias', $this->alias])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'keywords', $this->keywords])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['<>','status',Products::STATUS_NOT_ACTIVE])
            ->andFilterWhere(['<>','status',Products::STATUS_DELETE]);

        return $query->all();
    }
}
