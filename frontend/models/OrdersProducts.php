<?php
namespace frontend\models;

use Yii;


/**
 * This is the model class for table "orders_products".
 *
 * @property int $id
 * @property int $order_id
 * @property int $product_id
 * @property double $price
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Orders $order
 * @property Products $product
 */
class OrdersProducts extends \common\models\OrdersProducts
{
    const STATUS_PENDING = 1;
    const STATUS_CLOSED = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders_products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'product_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['price'], 'number'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'product_id' => 'Product ID',
            'price' => 'Price',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Get record by $product_id and $order_id
     * @param $product_id
     * @param $order_id
     * @return array|null|\yii\db\ActiveRecord
     */
    public function findExistProductInOrder($product_id, $order_id)
    {
        return self::find()
            ->where(['product_id' => $product_id])
            ->where(['order_id' => $order_id])
            ->one();
    }

    /**
     * @param $product_id
     * @param $order_id
     * @param int $status
     * @return array|null|\yii\db\ActiveRecord
     */
    public function getOrderProductByProductAndOrder($product_id, $order_id, $status=OrdersProducts::STATUS_PENDING )
    {
        return self::find()
            ->where(['product_id'=>$product_id])
            ->andWhere(['order_id'=>$order_id])
            ->andWhere(['status'=>$status])
            ->one();
    }

    /**
     * @param $product_id
     * @param $order_id
     * @param int $status
     * @return array|null|\yii\db\ActiveRecord
     */
    public function getOrderProductByIdAndOrder($id, $product_id, $order_id, $status=OrdersProducts::STATUS_PENDING )
    {
        return self::find()
            ->where(['id'=>$id])
            ->andWhere(['order_id'=>$order_id])
            ->andWhere(['product_id'=>$product_id])
            ->andWhere(['status'=>$status])
            ->one();
    }


}
