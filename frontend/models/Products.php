<?php

namespace frontend\models;

use common\models\Files;
use Yii;
use common\models\User;
use common\models\OrdersProducts;
use common\models\Category;
use common\models\DenominationOfWeights;

/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property int $category_id
 * @property string $code
 * @property string $title
 * @property string $alias
 * @property string $description
 * @property string $keywords
 * @property string $text
 * @property double $price
 * @property double $new_price
 * @property double $quantity
 * @property double $weight
 * @property int $denomination_of_weight_id
 * @property int $owner_id
 * @property int $status
 * @property int $main
 * @property int $recommended
 * @property int $like
 * @property int $dislike
 * @property int $created_at
 * @property int $updated_at
 *
 * @property OrdersProducts[] $ordersProducts
 * @property Category $category
 * @property DenominationOfWeights $denominationOfWeight
 * @property User $owner
 */
class Products extends \common\models\Products
{
    const STATUS_ACTIVE = 1;
    const STATUS_NOT_ACTIVE = 2;
    const STATUS_OVER = 3;
    const STATUS_DELETE = 4;
    const STATUS_PROMOTION = 5;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'code', 'title', 'description', 'keywords', 'denomination_of_weight_id', 'owner_id', 'created_at', 'updated_at'], 'required'],
            [['category_id', 'denomination_of_weight_id', 'owner_id', 'status', 'main', 'recommended', 'created_at', 'updated_at', 'dislike', 'like'], 'integer'],
            [['text'], 'string'],
            [['price', 'new_price', 'quantity', 'weight'], 'number'],
            [['code', 'title', 'alias', 'description', 'keywords'], 'string', 'max' => 255],
            [['alias'], 'unique'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['denomination_of_weight_id'], 'exist', 'skipOnError' => true, 'targetClass' => DenominationOfWeights::className(), 'targetAttribute' => ['denomination_of_weight_id' => 'id']],
            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['owner_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'code' => 'Code',
            'title' => 'Title',
            'alias' => 'Alias',
            'description' => 'Description',
            'keywords' => 'Keywords',
            'text' => 'Text',
            'price' => 'Price',
            'new_price' => 'New Price',
            'quantity' => 'Quantity',
            'weight' => 'Weight',
            'denomination_of_weight_id' => 'Denomination Of Weight ID',
            'owner_id' => 'Owner ID',
            'status' => 'Status',
            'main' => 'Main',
            'recommended' => 'Recommended',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function checkActiveProduct($id)
    {
        return self::find()
            ->where(['id'=>$id])
            ->andWhere(['status'=>Products::STATUS_ACTIVE])
            //->andWhere(['>=','quantity',1])
            ->one();
    }

}
