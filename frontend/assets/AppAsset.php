<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/navbar.css',
        'css/footer.css',
        'css/bootstrap/bootstrap.min.css',
        '//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css'
    ];
    public $js = [
        'js/bootstrap/bootstrap.min.js',
        'js/cart/cart.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
    /*
     * layout
     * public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'css/site.css',
        'css/bootstrap/bootstrap.min.css',
        'css/ie10-viewport-bug-workaround.css',
        'css/offcanvas.css',
    ];
    public $js = [
        'js/ie10-viewport-bug-workaround.js',
        'js/offcanvas.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
     */
}
