<?php
return [
    'adminEmail' => 'admin@products-drive.zp.ua',
    'user.rememberMeDuration' => 30 * 24 * 60
];
