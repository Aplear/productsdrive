$('.carousel').carousel({
    interval: 4000
});


$(document).ready(function() {
    $("div.pages.welcome").fadeIn("slow");

    $("a.list-group-item").on("click", function() {


        //If the element is not active
        if (!$(this).hasClass("active")) {
            $("a.active").removeClass("active");
            $(this).addClass("active");
            //Hide div page shown
            $("div.pages").hide();

            //List of div/pages to show or hide

            if ($(this).hasClass("home")) {
                $("div.pages.home").fadeIn("slow");
            }

            if ($(this).hasClass("welcome")) {
                $("div.pages.welcome").fadeIn("slow");
            }

            if ($(this).hasClass("tree1window1")) {
                $("div.pages.tree1window1").fadeIn("slow");
            }

            if ($(this).hasClass("tree1window2")) {
                $("div.pages.tree1window2").fadeIn("slow");
            }

            if ($(this).hasClass("reports")) {
                $("div.pages.reports").fadeIn("slow");
            }

            if ($(this).hasClass("settings")) {
                $("div.pages.settings").fadeIn("slow");
            }

            if ($(this).hasClass("open")) {
                $("div.pages.open").fadeIn("slow");
            }

            if ($(this).hasClass("tree2Window1")) {
                $("div.pages.tree2Window1").fadeIn("slow");
            }

            if ($(this).hasClass("tree2Window2")) {
                $("div.pages.tree2Window2").fadeIn("slow");
            }

        }
        //If clicked option with category class.
        if ($(this).hasClass("category"))   {
            //Collapses other opened div sibblings
            $("div").siblings(".panel-collapse.collapse.in").collapse("hide");
            //Unselects others options
            $("a.active").removeClass("active");
            //Opens first child of opened category div
            $(this).parent().parent().next().children().children().first().trigger( "click" );
        }
        else if ($(this).parent().parent().prev().children().children().hasClass("category")) {
            //Do nothing when selected children of opened category
        }
        //Collapses category when selected non-category option
        else if ($(".panel-collapse.collapse").hasClass("in")){
            $(".panel-collapse.collapse").collapse("hide");
        }
    });
});
