$(document).on('click', '.add-to-cart', function (e) {
    e.stopPropagation();
    e.preventDefault();
    var url = $(this).attr('href');
    $.ajax({
        type : "GET",
        url : url,
        //async:true,
        success : function (response) {
            $('.order-quantity').text(response).removeClass('hidden');
        },
        error : function (response) {
            return false;
        }
    });
});