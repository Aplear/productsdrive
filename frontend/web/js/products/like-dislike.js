$(document).on('click', '.like-dislike', function (e) {
    e.stopPropagation();
    e.preventDefault();
    var url = $(this).data('url');
    $.ajax({
        type : "GET",
        url : url,
        //async:true,
        success : function (response) {
        },
        error : function (response) {
            return false;
        }
    });
});