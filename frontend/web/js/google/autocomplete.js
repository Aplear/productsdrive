(function( $ ) {
    $.fn.googleAutocomplete = function() {
        //----------------------- START Autocomplete -------------------
        // Bind the map's bounds (viewport) property to the autocomplete object,
        // so that the autocomplete requests use the current map bounds for the
        // bounds option in the request.
        var defaultBounds = new google.maps.LatLngBounds(
            new google.maps.LatLng(47.8228900, 35.1903100)
            //new google.maps.LatLng(71, 51)
        );

        //set variables for identify each address field
        var input_field = {};
        var google_place = {};

        var options = {
            //bounds: defaultBounds,
            types: ['address'],
            //strictBounds: true,
            componentRestrictions: {country: 'ua'}
        };

        //google autocomplete place address
        this.each(function (index, item) {

            //stop submit form when choose address by [Enter]
            google.maps.event.addDomListener(item, 'keydown', function(event) {
                if (event.keyCode === 13) {
                    event.preventDefault();
                }
            });
            input_field[index] = new google.maps.places.Autocomplete(item,options);

            input_field[index].addListener('place_changed', function() {

                var district, lat, lng;
                google_place[index] = input_field[index].getPlace();
                var address = '';
                if (google_place[index].address_components) {
                    $.each(google_place[index].address_components,function(item,index){
                        if(index.types[0] === 'sublocality_level_1') {
                            $("#orders-district").val(index.long_name);
                            return false;
                        }
                    });
                }
                return false;

            });
        });

        //----------------------- END Autocomplete -------------------
    };
})(jQuery);