//Remove Product From Order
$(document).on('click', '.remove-product', function(e){
    e.stopPropagation();
    e.preventDefault();
    var url = $(this).data('url');
    var order_product_id = $(this).data('order-product-id');
    $.ajax({
        type : "GET",
        url : url,
        success : function (response) {
            if(response !== false) {
                $('.order-product-'+order_product_id).remove();
                updateShopingCartQuality(response.totalQuantity);
                updateTotalOrderPrice(response.totalPrice);
            } else {
                alert('Something wrong!')
            }
        },
        error : function (response) {
            return false;
        }
    });
});

//Change Quantity
$(document).on('change', '.change-quantity', function(e){
    e.stopPropagation();
    e.preventDefault();
    var quantity = $(this).val();
    var url = $(this).data('url')+'&'+$.param({'quantity':quantity});
    var order_product_id = $(this).data('order-product-id');
    $.ajax({
        type : "GET",
        url : url,
        success : function (response) {
            if(response !== false) {
                updateShopingCartQuality(response.totalQuantity);
                updateTotalOrderPrice(response.totalPrice);
            }
        },
        error : function (response) {
            return false;
        }
    });
});

function updateShopingCartQuality(quality) {
    $('.order-quantity').text(quality);
}

function updateTotalOrderPrice(price) {
    $('.total-order-price').text(price);
}