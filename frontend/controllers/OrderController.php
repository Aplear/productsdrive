<?php

namespace frontend\controllers;

use frontend\models\Orders;
use frontend\models\OrdersProducts;
use frontend\models\Products;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * OrderController
 */
class OrderController extends Controller
{
    public $orderSessionKey = null;

    /**
     * @param $id
     * @return bool|mixed
     * @throws NotFoundHttpException
     */
    public function actionAddProduct($id)
    {
        if(Yii::$app->request->isAjax) {
            $order = (new Orders())->getOrderBySessionKey();
            $product = (new Products)->checkActiveProduct($id);

            //check if product exist
            if(is_null($product)) {
                $session = Yii::$app->session;
                $session->setFlash('error', 'Product not found!');
                return false;
            }
            $ordersProduct = (new OrdersProducts)->getOrderProductByProductAndOrder($product->id,$order->id);
            //order product
            if(empty($ordersProduct) || is_null($ordersProduct)) {
                $ordersProduct = new OrdersProducts();
                $ordersProduct->order_id = $order->id;
                $ordersProduct->price = $product->price;
                $ordersProduct->product_id = $product->id;
                $ordersProduct->quantity++;
                $ordersProduct->status = OrdersProducts::STATUS_PENDING;
                $ordersProduct->save();
                unset($ordersProduct);
            } else {
                $ordersProduct->quantity++;
                $ordersProduct->price = ($ordersProduct->price+$product->price);
                $ordersProduct->save();
            }

            //order
            $order->quantity++;
            $order->save();
            unset($ordersProduct);
            unset($product);

            Yii::$app->response->format = Response::FORMAT_JSON;
            return $order->quantity;
        } else {
            throw new NotFoundHttpException();
        }

    }

    /**
     * @param $id
     * @return bool|mixed
     * @throws NotFoundHttpException
     */
    public function actionRemoveProduct($id)
    {
        if(Yii::$app->request->isAjax) {
            $orderModel = new Orders();
            $order = $orderModel->getOrderBySessionKey();
            $product = (new Products)->checkActiveProduct($id);
            Yii::$app->response->format = Response::FORMAT_JSON;

            //check if product exist
            if(is_null($product)) {
                $session = Yii::$app->session;
                $session->setFlash('error', 'Product not found!');
                return false;
            }
            $ordersProduct = (new OrdersProducts)->getOrderProductByProductAndOrder($id,$order->id);
            //order product
            if(!empty($ordersProduct) || !is_null($ordersProduct)) {
                //order
                $order->quantity = ($order->quantity-$ordersProduct->quantity);
                $order->save();
                $ordersProduct->delete();
                unset($ordersProduct);
                $data = $orderModel->updateAndGetTotalProductsPriceAndQuantity($order->id);
                return $data;
            }
            return false;
        } else {
            throw new NotFoundHttpException();
        }
    }
}
