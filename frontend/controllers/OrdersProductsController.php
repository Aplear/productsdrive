<?php

namespace frontend\controllers;

use frontend\models\Orders;
use frontend\models\Products;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use frontend\models\OrdersProducts;
use yii\web\Response;

/**
 * OrdersProductsController implements the CRUD actions for OrdersProducts model.
 */
class OrdersProductsController extends Controller
{
    public function actionChangeQuantity($id, $product_id, $quantity)
    {
        if(Yii::$app->request->isAjax) {
            $orderModel = new Orders();
            $order = $orderModel->getOrderBySessionKey();
            $product = (new Products)->checkActiveProduct($product_id);

            //check if product exist
            if(is_null($product)) {
                $session = Yii::$app->session;
                $session->setFlash('error', 'Product not found!');
                return false;
            }

            $orderProduct = (new OrdersProducts)->getOrderProductByIdAndOrder($id, $product_id, $order->id);
            //check if product exist
            if(is_null($orderProduct) || empty($orderProduct)) {
                $session = Yii::$app->session;
                $session->setFlash('error', 'Order Product not found!');
                return false;
            }

            //order
            $orderProduct->quantity = $quantity;
            $orderProduct->price = $product->price*$quantity;
            $orderProduct->save();

            $data = $orderModel->updateAndGetTotalProductsPriceAndQuantity($order->id);
            $data['orderProductTotal'] = $orderProduct->price;

            unset($orderProduct);
            unset($orderModel);
            unset($product);

            Yii::$app->response->format = Response::FORMAT_JSON;
            return $data;

        } else {
            throw new NotFoundHttpException();
        }
    }
}
