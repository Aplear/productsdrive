<?php

namespace frontend\controllers;

use frontend\models\Category;
use Yii;
use frontend\models\Products;
use frontend\models\search\ProductsSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProductsController implements the CRUD actions for Products model.
 */
class ProductsController extends MainController
{

    public function actionSearch()
    {
        $searchModel = new ProductsSearch();
        $products = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('search_result', [
            'products' => $products,
            'searchText' => $searchModel->title
        ]);
    }

    /**
     * Lists all Products models.
     * @param $alias
     * @return mixed
     * @throws HttpException
     * @internal param $category_alias
     */
    public function actionShort($alias)
    {
        $category = Category::findOne(['alias'=>$alias]);
        if(is_null($category)) {
            throw new HttpException(404 ,'Not found');
        }
        //get sub categories
        $categorySub = $category->getSubCategoriesId($category->id);
        $categorySub[$category->id] = $category->name;

        $products = Products::find()
            ->where(['in','category_id',array_keys($categorySub)])
            ->andWhere(['<>','status',Products::STATUS_NOT_ACTIVE])
            ->andWhere(['<>','status',Products::STATUS_DELETE])
            //->createCommand()->rawSql;
            ->all();

        return $this->render('short', [
            'products' => $products,
            'category' => $category
        ]);
    }

    public function actionFull($alias)
    {

        $product = Products::find()
            ->where(['alias'=>$alias])
            ->andWhere(['<>','status',Products::STATUS_NOT_ACTIVE])
            ->andWhere(['<>','status',Products::STATUS_DELETE])
            ->one();

        if(is_null($product)) {
            throw new HttpException(404 ,'Not found');
        }
        $category = Category::findOne([$product->category_id]);

        return $this->render('full', [
            'product' => $product,
            'category' => $category
        ]);
    }

    /**
     * @param $id
     */
    public function actionLike($id)
    {
        if(Yii::$app->request->isAjax)
        $product = Products::findOne($id);
        if(!empty($product) || !is_null($product)){
            $product->like++;
            $product->save();
        }
    }

    /**
     * @param $id
     */
    public function actionDislike($id)
    {
        if(Yii::$app->request->isAjax)
            $product = Products::findOne($id);
        if(!empty($product) || !is_null($product)){
            $product->dislike++;
            $product->save();
        }
    }
}
