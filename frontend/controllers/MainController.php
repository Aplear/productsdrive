<?php
namespace frontend\controllers;

use frontend\models\Pages;
use frontend\models\Category;
use frontend\models\Orders;
use frontend\models\search\ProductsSearch;
use Yii;
use yii\web\Controller;


/**
 * Site controller
 */
class MainController extends Controller
{
    public $layout = 'new';

    public function beforeAction($action)
    {
        $this->setOrderSessionKey();
        Yii::$app->view->params['orders'] = (new Orders())->getOrderBySessionKey();
        Yii::$app->view->params['pages'] = Pages::findAll(['status'=>Pages::STATUS_ACTIVE]);
        $productsSearchModel = new ProductsSearch();
        $dataProvider = $productsSearchModel->search(Yii::$app->request->queryParams);
        Yii::$app->view->params['productsSearchModel'] = $productsSearchModel;
        Yii::$app->view->params['categories'] = Category::findAll(['status'=>Category::STATUS_ACTIVE, 'parent_id'=>null]);
        return parent::beforeAction($action);
    }

    /**
     * Set order_session_key
     */
    private function setOrderSessionKey()
    {
        $session = Yii::$app->session;

        //check order session key && create if non set
        if (!$session->has('order_session_key')) {
            $order = new Orders();
            $order->order_session_key = $order->generateOrderSessionKey();
            $order->status = Orders::STATUS_ACTIVATED;
            $order->save();
        }
    }

}
