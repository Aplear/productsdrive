<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Pages;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PagesController implements the CRUD actions for Pages model.
 */
class PageController extends MainController
{
    /**
     * Lists all Pages models.
     * @param $alias
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionIndex($alias)
    {
        $page = Pages::find()->where(['alias'=>$alias])->one();
        if(is_null($page) || empty($page)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        return $this->render('index', [
            'page' => $page,
        ]);
    }
    /**
     * Finds the Pages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pages::findOne($id)) !== null) {
            return $model;
        }


    }
}
