<?php

namespace frontend\controllers;

use frontend\models\Orders;
use frontend\models\OrdersProducts;
use frontend\models\Products;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * OrderController
 */
class CheckoutController extends MainController
{
    public $orderSessionKey = null;

    public function actionIndex()
    {
        $model = new Orders();
        $order = $model->getOrderBySessionKey();
        $order->scenario = Orders::SCENARIO_CHECKOUT;

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if(Yii::$app->request->post() && $order->load(Yii::$app->request->post())) {
                return ActiveForm::validate($order);
            }
        }

        if($order->load(Yii::$app->request->post()) && $order->validate()) {
            //start transaction
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                $order->delivery_price += Orders::DEFAULT_DELIVERY_PRICE;
                $order->status = Orders::STATUS_WAITING_CONFIRM;
                $order->save();
                //close trunsation
                $transaction->commit();
            } catch (\Exception $e) {
                Yii::$app->session->setFlash('error', 'При оформлении заказа произошла ошибка');
                $transaction->rollback();
            }
            Yii::$app->session->setFlash('success', 'Ваш заказ принят, в тичении 5 минут мы перезвоним вам для уточнения заказа');
            return $this->redirect('/index');
        }

        return $this->render('/order/checkout_form', [
            'order' => $order
        ]);
    }
}
