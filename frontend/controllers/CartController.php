<?php

namespace frontend\controllers;

use frontend\models\Orders;
use frontend\models\OrdersProducts;
use Yii;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CartController extends MainController
{


    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex()
    {
        $order = (new Orders())->getOrderBySessionKey();

        return $this->render('index', [
            'order' => $order,
            'orderProducts' => $order->ordersProducts,
        ]);
    }

}
