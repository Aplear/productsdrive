<?php

use yii\bootstrap\ActiveForm;
use frontend\models\Products;
use \frontend\models\Orders;
use yii\bootstrap\Html;

?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Оформление Заказа</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="row col-md-6">
                    <?php $form = ActiveForm::begin([
                        'id' => 'checkoutOrder',
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => true,
                        'fieldConfig' => [
                            'options' => [],
                            'template' => '{label}{input}{error}',
                            'inputOptions' => [
                                'class'=>'validate form-control',
                            ]
                        ],
                    ]); ?>

                    <?= $form->field($order, 'first_name')->textInput([
                        'maxlength' => true,
                        'placeholder' => 'Имя'
                    ])->label(false) ?>

                    <?= $form->field($order, 'last_name')->textInput([
                        'maxlength' => true,
                        'placeholder' => 'Фамилия'
                    ])->label(false) ?>

                    <?= $form->field($order, 'phone')->textInput([
                        'maxlength' => true,
                        'placeholder' => '+380(00)000-00-00'
                    ])->label(false) ?>

                    <?= $form->field($order, 'email')->textInput([
                        'maxlength' => true,
                        'placeholder' => 'Email'
                    ])->label(false) ?>

                    <?= $form->field($order, 'address')->textInput([
                        'maxlength' => true,
                        'placeholder' => 'Адресс'
                    ])->label(false) ?>

                    <?= $form->field($order, 'district')->hiddenInput()->label(false) ?>

                    <?= $form->field($order, 'apartment_number')->textInput([
                        'maxlength' => true,
                        'placeholder' => 'Номер квартиры'
                    ])->label(false) ?>

                    <p style="color:orange;">Укажите номер квартиры если это жилой комплекс</p>

                    <?= $form->field($order, 'addition_text')->textarea([
                        'rows' => 6,
                        'placeholder' => 'Дополнительная информация к заказу'
                    ])->label(false) ?>

                    <div class="form-group">
                        <?= Html::submitButton('Оформить', ['class' => 'btn btn-success']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
                <div class="row col-md-6 pull-right">
                    <div class="panel panel-info">
                        <div class="panel-body">
                             Стоимость доставки <b><?=Orders::DEFAULT_DELIVERY_PRICE?> <?=Products::CURRENCY?></b>
                        </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-body">
                            Оплата при доставке куръеру <b><?=$order->products_total_price+Orders::DEFAULT_DELIVERY_PRICE?> <?=Products::CURRENCY?></b>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->registerJsFile('https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyBCfwZW1fEASM0PheSF08KRWlzVric-gsw&language=ru&libraries=places', [
    'position' => \yii\web\View::POS_END,
    'defer' => true
]); ?>
<?php $this->registerJsFile('@web/js/google/autocomplete.js', [
    'position' => \yii\web\View::POS_END,
    'defer' => true
]); ?>

<?php $this->registerJsFile('@web/js/libs/jquery.inputmask.js', [
    'position' => \yii\web\View::POS_END,
    'defer'=>true
]); ?>
<?php $this->registerJsFile('@web/js/libs/jquery.inputmask-multi.js', [
    'position' => \yii\web\View::POS_END,
    'defer'=>true
]); ?>
<?php $this->registerJsFile('@web/js/checkout/checkout.js', [
    'position' => \yii\web\View::POS_END,
    'defer' => true
]); ?>