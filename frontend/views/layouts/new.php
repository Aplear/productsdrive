<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use \yii\helpers\Url;
use \frontend\models\Products;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="robots" content="none"/>
    <meta name="googlebot" content="noindex, nofollow"/>
    <meta name="yandex" content="none"/>

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top">

            <div class="container">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="/">
                            <?=Yii::$app->name?>
                        </a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <?php echo $this->render('/products/_search', ['model' => $this->params['productsSearchModel']]); ?>
                            </li>

                            <?php if (Yii::$app->user->isGuest):?>
                                <li>
                                    <a href="<?=Url::to(['/contacts'])?>">Contacts</a>
                                </li>
                                <li>
                                    <a href="<?=Url::to(['/site/signup'])?>">Sign-up</a>
                                </li>
                                <li>
                                    <a href="<?=Url::to(['/site/login'])?>">Login</a>
                                </li>
                            <?php else:?>

                                <!-- notification dropdown start-->
                                <li id="header_notification_bar" class="dropdown">
                                    <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false">
                                        <i class="glyphicon glyphicon-bell"></i>
                                        <span class="badge bg-warning">0</span>
                                    </a>
                                    <ul class="dropdown-menu extended notification">
                                        <li>
                                            <p>Notifications</p>
                                        </li>
                                        <li>
                                            <!--<div class="alert alert-info clearfix">
                                                <span class="alert-icon"><i class="fa fa-bolt"></i></span>
                                                <div class="noti-info">
                                                    <a href="#"> Server #1 overloaded.</a>
                                                </div>
                                            </div>-->
                                        </li>

                                    </ul>
                                </li>
                                <!-- notification dropdown end -->

                                <li class="dropdown ">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        <?=Yii::$app->user->identity->username?>
                                        <span class="caret"></span></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li class="dropdown-header">SETTINGS</li>
                                        <!--<li class=""><a href="#">Other Link</a></li>
                                        <li class=""><a href="#">Other Link</a></li>
                                        <li class=""><a href="#">Other Link</a></li>
                                        <li class="divider"></li>-->
                                        <li>
                                            <?=Html::beginForm(['/site/logout'], 'post')
                                            . Html::submitButton(
                                                'Logout',
                                                ['class' => 'btn btn-link logout']
                                            )
                                            . Html::endForm()?>
                                        </li>
                                    </ul>
                                </li>
                            <?php endif;?>

                            <!-- settings start -->
                            <li class="dropdown">
                                <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false">
                                    <i class="glyphicon glyphicon-shopping-cart"></i>
                                    <?php if(isset($this->params['orders'])):?>
                                        <span class="order-quantity badge badge-primary">
                                            <?=$this->params['orders']->quantity?>
                                        </span>
                                    <?php else:?>
                                        <span class="order-quantity badge badge-primary hidden"></span>
                                    <?php endif;?>
                                </a>
                                <?php if(isset($this->params['orders']->quantity)):?>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li class="dropdown-header"> В корзине <?=$this->params['orders']->quantity?> товар(а)</li>
                                        <?php if(!empty($this->params['orders']->ordersProducts)):?>
                                            <?php foreach ($this->params['orders']->ordersProducts as $orderProducts):?>
                                                <?php $product = $orderProducts->product;?>
                                                <li>
                                                    <a href="<?=Url::to(['/product/'.$product->alias])?>">
                                                        <div class="task-info clearfix">
                                                            <div class="desc pull-left">
                                                                <h5><?=$product->title?></h5>
                                                            </div>
                                                            <span class="notification-pie-chart pull-right" data-percent="78">
                                                                <span class="price">Цена <?=$product->price?> <?=\frontend\models\Products::CURRENCY?></span>
                                                            </span>
                                                        </div>
                                                    </a>
                                                </li>
                                            <?php endforeach;?>
                                        <?php else:?>
                                        <?php endif;?>
                                        <li role="separator" class="divider"></li>
                                        <li class="external">
                                            <a class="btn-default" href="<?=Url::to(['/cart'])?>">В Корзину</a>
                                        </li>
                                    </ul>
                                <?php else:?>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li role="separator" class="divider"></li>
                                        <li class="external">
                                            <a class="btn-default" href="<?=Url::to(['/cart'])?>">В Корзину</a>
                                        </li>
                                    </ul>
                                <?php endif;?>

                            </li>
                            <!-- settings end -->

                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </div>

        </nav>

    <!-- Page Content -->
    <div class="container">

        <div class="row row-offcanvas row-offcanvas-right ">
            <div class="col-md-12">
                <div class="col-md-3">
                    <?=$this->render('/category/index',[
                        'model' => $this->params['categories']
                    ])?>
                </div>
                <!-- /.col-lg-3 -->
                <div class="col-md-9">
                    <?= Alert::widget() ?>
                    <?= Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
                    <?= $content ?>
                </div>
                <!-- /.col-lg-9 -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- Footer -->
        <footer class="py-5 bg-dark">
            <hr />
            <?=$this->render('/site/footer',[
                'pages'=>$this->params['pages']
            ])?>

            <div class="container">
                <p class="m-0 text-center text-white">Copyright &copy; Products Drive 2018</p>
            </div>
            <!-- /.container -->
        </footer>
    </div>
    <!-- /.container -->
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>