<?php
use yii\helpers\Url;
?>
<div class="container">

    <div class="row"><!-- row -->

        <div class="col-lg-6 col-md-6"><!-- widgets column left -->

            <ul class="list-unstyled clear-margins"><!-- widgets -->

                <li class="widget-container widget_nav_menu"><!-- widgets list -->

                    <h3 class="title-widget">Полезные ссылки</h3>

                    <ul>
                        <?php foreach ($pages as $page):?>
                            <li><a href="<?=Url::to(['/page/'.$page->alias])?>"><i class="fa fa-angle-double-right"></i> <?=$page->title?></a></li>
                        <?php endforeach;?>
                    </ul>

                </li>

            </ul>

        </div><!-- widgets column left end -->

        <div class="col-lg-6 col-md-6"><!-- widgets column center -->

            <ul class="list-unstyled clear-margins"><!-- widgets -->

                <li class="widget-container widget_recent_news"><!-- widgets list -->

                    <h3 class="title-widget">Контактная Информация </h3>

                    <div class="footerp">

                        <h4 class="title-median">Products Drive</h4>
                        <p><b>Email:</b> <a href="mailto:info@webenlance.com">productdrivezp@gmail.com</a></p>
                        <p><b>Контакнтые номера </b>

                            <b style="color:#ffc106;">(с 6:00 - до 23:00):</b>  +380(66)2561978, +380(66)3883825  </p>

                        <!--<p><b>Corp Office / Postal Address</b></p>
                        <p><b>Phone Numbers : </b>7042827160, </p>
                        <p> 011-27568832, 9868387223</p>-->
                    </div>

                    <div class="social-icons">

                        <div class="text-center center-block">
                            <a href="https://www.facebook.com/Products-Drive-1000000733481968/?modal=admin_todo_tour"><i id="social-fb" class="fa fa-facebook-square fa-3x social"></i></a>
                            <a href="https://twitter.com/bootsnipp"><i id="social-tw" class="fa fa-twitter-square fa-3x social"></i></a>
                            <a href="https://www.instagram.com/productdrivezp/?hl=ru"><i id="social-i" class="fa fa-instagram fa-3x social"></i></a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>

    </div>
</div>