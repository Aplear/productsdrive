<?php
use yii\helpers\Html;
use frontend\models\Products;
use \yii\helpers\Url;
/* @var $orderProducts frontend\models\Orders */
?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">
                    <div class="row">
                        <div class="col-md-12">
                            <h4><span class="glyphicon glyphicon-shopping-cart"></span> Ваша корзина</h4>
                        </div>
                        <!--<div class="col-xs-6">
                            <button type="button" class="btn btn-default btn-sm btn-block">
                                <span class="glyphicon glyphicon-share-alt"></span> Продолжить покупку
                            </button>
                        </div>-->
                    </div>
                </div>
            </div>


                <?php if(empty($orderProducts) || is_null($orderProducts)):?>
                <div class="panel-body">
                        <h3>Пусто</h3>
                </div>
                <?php else:?>
                    <div class="panel-body">

                    <?php foreach ($orderProducts as $orderProduct):?>
                        <div class="row order-product-<?=$orderProduct->id?>">

                                <div class="col-md-2 text-center">
                                    <a href="#">
                                        <img class="inline-block" src="<?=$orderProduct->product->productFile->link?>" alt="<?=$orderProduct->product->productFile->alt?>" width="50">
                                    </a>
                                </div>
                                <div class="col-md-5">
                                    <h4><?=$orderProduct->product->title?></h4>
                                    <h5 class="label label-warning"><?=$orderProduct->product->price?> <?=Products::CURRENCY?></h5>
                                </div>

                                <div class="col-md-2 text-right">
                                    <h6>
                                        <strong>
                                            <span class="produc-total-price">
                                                <?=$orderProduct->product->price?> <?=Products::CURRENCY?>
                                            </span>
                                            <span class="text-muted">x</span>
                                        </strong>
                                    </h6>
                                </div>
                                <div class="col-md-2">
                                    <?=HTML::input('number','Orders', $orderProduct->quantity, [
                                        'class'=>'form-control input-sm change-quantity',
                                        'min' => 1,
                                        'data-url' => Url::to(['/orders-products/change-quantity', 'id'=>$orderProduct->id, 'product_id'=>$orderProduct->product->id])
                                    ]);?>
                                </div>
                                <div class="col-md-1">
                                    <button type="button" class="btn btn-link btn-xs btn-default remove-product"
                                            data-order-product-id="<?=$orderProduct->id?>"
                                            data-url="<?=Url::to(['/order/remove-product', 'id'=>$orderProduct->product->id])?>">
                                        <span class="glyphicon glyphicon-remove-circle"> </span>
                                    </button>
                                </div>

                        </div>
                        <hr />
                    <?php endforeach;?>
                    </div>
                    <div class="panel-footer">
                        <div class="row text-center">
                            <div class="col-xs-8">
                                <h4 class="text-right">Итого <strong class="total-order-price"><?=$order->products_total_price?></strong> <strong><?=Products::CURRENCY?></strong></h4>
                            </div>
                            <div class="col-xs-4">
                                <a href="<?=Url::to(['/order/checkout'])?>" role="button" class="btn btn-success btn-block">
                                    Оформить заказ
                                </a>
                            </div>
                        </div>
                    </div>
                <?php endif;?>
        </div>
    </div>
</div>