<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use \yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\ProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $category common\models\Category */
/* @var $products common\models\Products */

$this->title = 'Поиск - '.$searchText;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mt-4">
    <div class="row">
        <?php foreach ($products as $product):?>
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <img src="<?=$product->productFile->link?>" alt="<?=$product->productFile->alt?>" width="100"/>
                    <div class="caption">
                        <h4>
                            <a href="<?=Url::to(['/product/'.$product->alias])?>" class="" >
                                <?=$product->title?>
                            </a>
                        </h4>
                        <p><?=mb_substr($product->text, 0,70)?>...</p>
                        <p><a href="#" class="btn btn-success glyphicon glyphicon-shopping-cart" role="button"></a></p>
                    </div>
                </div>
            </div>
        <?php endforeach;?>
    </div>
</div>