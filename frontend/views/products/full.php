<?php
/* @var $category common\models\Category */

use yii\helpers\Url;
use \frontend\models\Products;

/* @var $product common\models\Products */
$this->title = $product->title;

$this->params['breadcrumbs'][] = ['label' => $category->name, 'url' => ['/category/'.$category->alias]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid">
    <div class="content-wrapper">
        <div class="item-container">
            <div class="container">
                <div class="col-md-5">
                    <div class="product col-md-12 service-image-left">

                        <center>
                            <img class="show-product-image-preview" id="item-display" src="<?=$product->productFile->link?>" alt="<?=$product->productFile->alt?>" />
                        </center>
                    </div>

                </div>

                <div class="col-md-7">
                    <div class="product-title"><?=$product->title?></div>
                    <div class="product-desc"><?=$product->description?></div>
                    <div class="product-rating">
                        <button data-url="<?=Url::to(['/products/like','id' => $product->id,])?>" class="btn btn-default glyphicon glyphicon-thumbs-up like-dislike" role="button"><?=$product->like?></button>
                        <button data-url="<?=Url::to(['/products/dislike','id' => $product->id,])?>" class="btn btn-default glyphicon glyphicon-thumbs-down like-dislike" role="button"><?=$product->dislike?></button>
                    </div>
                    <hr>
                    <div class="product-price"><?=$product->price?> <?=Products::CURRENCY?></div>
                    <div class="product-stock">В наличии</div>
                    <hr>
                    <div class="btn-group cart">
                        <a href="<?=Url::to(['/order/add-product','id' => $product->id,])?>" class="btn btn-success glyphicon glyphicon-shopping-cart add-to-cart" role="button">
                            В корзину
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="col-md-12 product-info">
                <ul id="myTab" class="nav nav-tabs nav_tabs">

                    <li class="active"><a href="#service-one" data-toggle="tab">DESCRIPTION</a></li>
                    <li><a href="#service-two" data-toggle="tab">PRODUCT INFO</a></li>
                    <li><a href="#service-three" data-toggle="tab">REVIEWS</a></li>

                </ul>
                <div id="myTabContent" class="tab-content">
                    <div class="tab-pane fade in active" id="service-one">

                        <section class="container product-info">
                            <?=$product->text?>
                        </section>

                    </div>
                    <div class="tab-pane fade" id="service-two">

                        <section class="container product-info">
                            <?=$product->product_info?>
                        </section>

                    </div>
                    <div class="tab-pane fade" id="service-three">

                    </div>
                </div>
                <hr>
            </div>
        </div>
    </div>
</div>
<!-- /.product -->
<?php
    $this->registerJsFile("@web/js/products/add_to_cart.js",[
        'position' => \yii\web\View::POS_END,
        'defer' => true
    ]);
?>