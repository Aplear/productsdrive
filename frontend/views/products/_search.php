<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\search\ProductsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="products-search">

    <?php $form = ActiveForm::begin([
        'action' => ['/products/search'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1,
            'class' => 'navbar-form navbar-left'
        ],
    ]); ?>

    <?= $form->field($model, 'title',['template'=>'{input}'])->textInput([
            'placeholder'=>'Поиск'
    ])->label(false) ?>

    <?= Html::submitButton('<i class="glyphicon glyphicon-search"></i>', [
        'class' => 'btn btn-default'
    ]) ?>

    <?php ActiveForm::end(); ?>

</div>
