<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use \yii\helpers\Url;
use frontend\models\Products;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\ProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $category common\models\Category */
/* @var $products common\models\Products */

$this->title = 'Продукты из категории '.mb_strtolower($category->name);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mt-4">
    <div class="row">
        <?php foreach ($products as $product):?>
                <div class="col-sm-6 col-md-4">
                    <div class="thumbnail">
                        <?php if(isset($product->productFile)):?>
                            <img class="show-product-image-preview" src="<?=$product->productFile->link?>" alt="<?=$product->productFile->alt?>"/>
                        <?php endif;?>
                        <div class="caption">
                            <h4>
                                <a href="<?=Url::to(['/product/'.$product->alias])?>" class="" >
                                    <?=$product->title?>
                                </a>
                            </h4>
                            <p><?=mb_substr($product->text, 0,70)?>...</p>
                            <p>
                                <a href="<?=Url::to(['/order/add-product','id' => $product->id,])?>" class="btn btn-success glyphicon glyphicon-shopping-cart add-to-cart" role="button"></a>
                                <button  class="btn btn-warning" role="button"><?=$product->price?> <?=Products::CURRENCY?></button>
                            </p>
                        </div>
                    </div>
                </div>
        <?php endforeach;?>
    </div>
</div>

<?php
    $this->registerJsFile("@web/js/products/add_to_cart.js",[
        'position' => \yii\web\View::POS_END,
        'defer' => true
    ]);
?>