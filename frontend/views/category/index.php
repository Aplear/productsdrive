<?php
use \frontend\models\Category;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\models\Category */

?>

<div class="list-group">
    <?php foreach ($model as $item):?>
        <a href="<?=Url::to(['/category/'.$item->alias])?>" class="list-group-item"><?=$item->name?></a>
    <?php endforeach;?>
</div>

<nav class="list-group hidden">
    <ul class="nav">
        <li>
            <a class="list-group-item" href="#">Link 1</a>
        </li>
        <li>
            <a class="list-group-item" href="#">Link 2</a>
            <a href="#" class="toggle-custom" id="btn-1" data-toggle="collapse" data-target="#submenu1" aria-expanded="false">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
            </a>
            <ul class="nav collapse" id="submenu1" role="menu" aria-labelledby="btn-1">
                <li>
                    <a class="list-group-item" href="#">Link 2.1</a>
                </li>
                <li>
                    <a class="list-group-item" href="#">Link 2.2</a>
                </li>
                <li>
                    <a class="list-group-item" href="#">Link 2.3</a>
                </li>
            </ul>
        </li>
        <li>
            <a class="list-group-item"  href="#">Link 3</a>
        </li>
    </ul>
</nav>

<div class="dropdown list-group hidden">
    <?php foreach ($model as $item):?>
        <?php $subCategories = Category::getByParent($item->id);?>
        <?php if(is_null($subCategories) || empty($subCategories)):?>
            <a href="<?=Url::to(['/category/'.$item->alias])?>" class="list-group-item"><?=$item->name?></a>
        <?php else:?>
            <button class="btn btn-default dropdown-toggle list-group-item" type="button" id="dropdownMenu<?=$item->id?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <?=$item->name?>
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu<?=$item->id?>">
                <?php foreach ($subCategories as $subCategory):?>
                    <a href="<?=Url::to(['/category/'.$subCategory->alias])?>" class="list-group-item"><?=$subCategory->name?></a>
                <?php endforeach;?>
            </ul>
        <?php endif;?>
    <?php endforeach;?>
</div>

